<?php
	require_once("cache.php");
	require_once("conf.php"); 
	include_once("page_template.html");
	include_once("aplicaciones/dbcon.php");
?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <?php
						 // realizamos la conexion mediante odbc
						 // $cid = odbc_connect($dsn, $usuario, $clave);
							$cid = odbc_connect('SREL', 'sa', 'SRel9300');

							if (!$cid){
								exit("<strong>Ha ocurrido un error tratando de conectarse con la base de datos.</strong>");
							}	

							$marca = $_REQUEST['marca'];

							if ($_GET['marca']<>"") {
								echo '<div id="contentliquid"><div id="contentwrap"><div id="content">';
								$dg = new C_DataGrid("SELECT id, codVehiculo, marca, modelo, descrip, motor, ano, chasis, obs, mantencion 
													  FROM dbo.vehiculos", "id", "vehiculos");
								$dg -> set_query_filter ("marca = '$marca'");

								$dg -> set_col_dynalink("descrip", "modelo.php", "codVehiculo", "&marca=", "_self");
								$dg -> set_col_link("mantencion"); 

								$dg -> set_col_width("codVehiculo", 120);
								$dg -> set_col_width("descrip", 270);
								$dg -> set_col_width("marca", 120);
								$dg -> set_col_width("modelo", 120);
								$dg -> set_col_width("motor", 100);
								$dg -> set_col_width("ano", 120);
									
								$dg -> set_col_readonly("img");
								$dg -> set_col_hidden ("id",FALSE);

								$dg -> enable_search(true);
								$dg -> set_theme('aristo');
									
								$dg -> set_col_title("codVehiculo", "Código Vehículo");
								$dg -> set_col_title("marca", "Marca");
								$dg -> set_col_title("modelo", "Modelo");
								$dg -> set_col_title("descrip", "Descripción");
								$dg -> set_col_title("motor", "Motor");
								$dg -> set_col_title("ano", "Desde/Hasta");
								$dg -> set_col_title("chasis", "VIN");
								$dg -> set_col_title("obs", "Observación");
								$dg -> set_col_title("mantencion", "Imagen");
								$dg -> set_col_link("mantencion"); 

								$dg -> enable_edit("FORM","CRUD");
								$dg -> set_col_img("img");
								$dg -> enable_export('EXCEL');
								$dg -> set_dimension(1100, 420);
								$dg -> display();

								echo '</div></div></div>';
							}

							$modelo = $_GET['codVehiculo'];
							$nombre = filter_input(INPUT_GET, 'modelo');
							if ($modelo!="X") { 
								echo '<div id="contentliquid"><div id="contentwrap"><div id="content">';
									$sql = "SELECT codVehiculo, descrip, motor, ano 
										FROM Srel.dbo.vehiculos 
										WHERE codVehiculo = '$modelo' 
										ORDER BY descrip";
										
								$rs = odbc_exec($dbcon, $sql);
								while($row = odbc_fetch_array($rs)) {
									echo '<option value="'.$row['codVehiculo'].'">Vehiculo Seleccionado: '.$row['descrip'].' - '.$row['motor'].'-'.$row['ano'].' Cod:'.$row['codVehiculo'].'</option>';	
								}

								$dg = new C_DataGrid ("SELECT prod_vehiculos.CodProd, CodigoBarra, DesProd2, CodSubGru, DesProd, Stock, PrecioBol, CodGrupo, Ubicacion 
													   FROM ViewStockSrelProd INNER JOIN prod_vehiculos ON prod_vehiculos.CodProd = ViewStockSrelProd.CodProd", "CodProd", "ViewStockSrelProd");
								$dg -> set_query_filter ("prod_vehiculos.codVehiculo = '$modelo'");     

								$dg -> set_col_hidden("CodGrupo");
								$dg -> set_col_title("CodProd", "Código");
								$dg -> set_col_title("DesProd2", "OEM");
								$dg -> set_col_title("CodSubGr", "Sub grupo");
								$dg -> set_col_title("DesProd", "Descripción");
								$dg -> set_col_title("Stock", "Stock");
								$dg -> set_col_title("PrecioBol", "Precio con IVA");
								$dg -> set_col_title("Ubicacion ", "Ubicación");

								$dg -> set_col_width("CodProd", 105);
								$dg -> set_col_width("CodigoBarra", 100);
								$dg -> set_col_width("DesProd2", 110);
								$dg -> set_col_width("CodSubGru", 90);
								$dg -> set_col_width("DesProd", 390);
								$dg -> set_col_width("Stock", 70);
								$dg -> set_col_width("PrecioBol", 70);
								$dg -> set_col_edittype ("DesProd", "textarea");

								$dg -> set_col_edittype("CodSubGru", "select", "AMO:Amortiguadores;CAR:Carroceria;COR:Correas;FIL:Filtros;EMB:Embrague;ENC:Encendido;FRE:Frenos;DIS:Distribucion;MOT:Motor;EMP:Empaquetaduras;REF:Refrigeracion;SUS:Suspension;TRA:Transmision;UNI:Universal;LUB:Lubricantes", false);
								$dg -> enable_search(true);
								$dg -> set_theme('aristo');
								$dg -> set_dimension(1000,420);
								$dg -> set_locale('es');
								$dg -> set_multiselect(true);
								$dg -> enable_export('excel');
								$dg -> display();

								echo '</div></div></div>';
							} 

							$sql = "SELECT CodGrupo, DesGrupo FROM Srel.softland.iw_tgrupo WHERE DesGrupo NOT LIKE '%x%'ORDER BY DesGrupo ";
							$result = odbc_exec($cid, $sql)or die(exit("Error en conexión"));
						?>
						<div id="contMain" name="contMain" style="height:1px;width:1px"></div>
					</div>
				</div>
			</div>
		</div>
    <!-- jQuery -->
		<script src="js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
			$('#page-wrapper').click(function(){
				var rows = getSelRows();
				$("#contMain").load("main.php?cod="+rows);
			});
		</script>
	</body>
</html>