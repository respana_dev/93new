<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include_once("bin/funciones.php");

$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');

$codigo=$_GET["cod"];
$accion=$_GET["ac"];

$valorbusqueda=$_GET["valorbusqueda"];
$val_B=$_GET["b"];
$val_C=$_GET["c"];
$val_D=$_GET["d"];

$idVendedor=$_GET["idvend"];
$idRut=$_GET["idrut"];
$valdcto=$_GET["valdcto"];
$idProv=$_GET["selProv"];
$codProv=$_GET["codProv"];

$codigos = array();
$contenido = array();
$codigos = explode(",",$codigo);
$cod=$codigo-1;

if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}
if($accion=="vend") {
	$_SESSION['idvend'] = $idVendedor;
}
if($accion=="prov") {
	$_SESSION['idProv'] = $idProv;
}

$acc = $_POST['acc'];
if($acc=="guardarcliente") {
	$idRut = $_POST["rut"];
	$nom = $_POST["nom"];
	$comu = $_POST["comu"];
	$dir = $_POST["dir"];
	$idcliente2 = substr($idRut,0,strpos($idRut,"-"));
	$sql="SELECT * FROM softland.cwtauxi WHERE (CodAux = '$idcliente2')";
	$rs = odbc_exec($dbcon,$sql);
	$row = odbc_fetch_array($rs);
	if(count($row)>1) {
		$sql = "UPDATE softland.cwtauxi SET NomAux = '$nom', NoFAux = '$nom', ComAux = '$comu', DirAux = '$dir' WHERE (CodAux = '$idcliente2')";
		$rs = odbc_exec($dbcon,$sql);
	}else {
		$sql = "INSERT INTO softland.cwtauxi (CodAux, NomAux, NoFAux, RutAux, ActAux, ComAux, DirAux, esReceptorDTE) VALUES ('$idcliente2','$nom','$nom','$idRut','S','$comu','$dir','N')";
		$rs = odbc_exec($dbcon,$sql);
	}
}
if($accion=="dctoG") {
	$_SESSION['dctoGen'] = $valdcto;
	
	$valorTotal=0;
	$valx=0;
	foreach($Matrix as $key=>$row) {
		$valorTotal=$row[7] + $valorTotal;
	}
	if($valdcto!=0) {
		$valx=strpos($valdcto,'%');
	}
	if($valx!=0) {
		$res=substr($valdcto,0,$valx);
		if($res<=15) {
			$res2=$valorTotal-(($valorTotal*$res)/100);
			$valE=$res2;
		} else {
			$_SESSION['dctoGen'] = 0;
			alert("No puede dar un descuento superior al 15%");
			echo '
			<script>
			document.getElementById(\'txtDctoG\').value = \'0\';
			document.getElementById(\'txtTotG\').value = \''.$valorTotal.'\';
			</script>
			';
			return;
		}
	} else {
		if($valdcto<=($valorTotal*0.15)) {
			$valE=$valorTotal-$valdcto;
		} else {
			$_SESSION['dctoGen'] = 0;
			alert("No puede dar un descuento superior al 15%");
			echo '
			<script>
			document.getElementById(\'txtDctoG\').value = \'0\';
			document.getElementById(\'txtTotG\').value = \''.$valorTotal.'\';
			</script>
			';
			return;
		}
	}
}
if($accion=="del") {
	$cod=$codigo-1;
	del_array($cod);
}
if($accion=="sum") {
	$Matrix[$cod][5]=$val_B;
	$Matrix[$cod][6]=$val_C;
	$Matrix[$cod][7]=$val_D;
	$accion="sumG";
	
}
if($ac="save") {
	$ccod=$Matrix[$cod][0];
	$idProv = $_SESSION['idProv'];
	if($codProv!='') {
		$sql = "DELETE FROM CodProv WHERE (codProd = '$ccod') AND (CodAux = '$idProv')";
		$rs = odbc_exec($dbcon,$sql);	
		$sql = "INSERT INTO CodProv VALUES('$idProv','$ccod','$codProv')";
		$rs = odbc_exec($dbcon,$sql);
	}
	$Matrix[$cod][8] = $codProv;
	$_SESSION['Matrix'] = $Matrix;
}
if($accion=="limpiar") {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
	$_SESSION['idrut'] = '';
	session_destroy();
}
if($accion=="bus") {
	$cod=$codigo-2;
	if($valorbusqueda!='') { 
		$idProv = $_SESSION['idProv'];
		$sql = "SELECT CodProd, CodSubGru, DesProd, Stock, PrecioBol FROM ViewStockSrelProd WHERE (CodProd = '$valorbusqueda') OR (CodigoBarra = '$valorbusqueda')";
		$rs = odbc_exec($dbcon,$sql);
		$row = odbc_fetch_array($rs);
		$cantidad = count($row);
		//alert($cantidad);
		if($cantidad>2) {
			$codProv=buscaCodProv($row['CodProd'],$idProv);
			echo '
			<script>
			document.getElementById(\'txtBotonC\').disabled = false;
			document.getElementById(\'txtCart1\').value = \''.$row['CodProd'].'\';
			document.getElementById(\'txtCart3\').value = \''.$codProv.'\';
			document.getElementById(\'txtCart4\').value = \''.$row['DesProd'].'\';
			document.getElementById(\'txtCart5\').value = \''.number_format($row['Stock']).'\';
			document.getElementById(\'txtCart6\').value = \''.$row['PrecioBol'].'\';
			document.getElementById(\'txtCart7\').value = \'1\';
			document.getElementById(\'txtCart9\').value = \''.$row['PrecioBol'].'\';
			document.getElementById(\'txtTotG\').value = \'\';
			document.getElementById(\'txtCart3\').disabled = false;
			document.getElementById(\'txtCart4\').disabled = false;
			document.getElementById(\'txtCart5\').disabled = false;
			document.getElementById(\'txtCart6\').disabled = false;
			document.getElementById(\'txtCart7\').disabled = false;
			document.getElementById(\'txtCart8\').disabled = false;
			document.getElementById(\'txtCart9\').disabled = false;
			document.getElementById(\'txtTotG\').disabled = false;
			</script>';
			add_array(array($row['CodProd'],$row['CodSubGru'],$row['DesProd'],$row['Stock'],$row['PrecioBol'],'1','0',$row['PrecioBol'],$codProv),0,$_SESSION["SESS_item"]);
		} else {
			echo '
			<script>
			document.getElementById(\'txtCart1\').value = \'\';
			document.getElementById(\'txtCart3\').value = \'\';
			document.getElementById(\'txtCart4\').value = \'\';
			document.getElementById(\'txtCart5\').value = \'\';
			document.getElementById(\'txtCart6\').value = \'\';
			document.getElementById(\'txtCart8\').value = \'\';
			document.getElementById(\'txtCart9\').value = \'\';
			</script>';	
		}
	}
}

if($accion=="sumG") {
	$valorTotal=0;
	$valx=0;
	foreach($Matrix as $key=>$row) {
		$valorTotal=$row[7] + $valorTotal;
	}
	if($valdcto!=0) {
		$valx=strpos($valdcto,'%');
	}
	if($valx!=0) {
		$res=substr($valdcto,0,$valx);
		$res2=$valorTotal-(($valorTotal*$res)/100);
		$valE=$res2;
	} else {
		$valE=$valorTotal-$valdcto;
	}
	
}
$_SESSION['Matrix']=$Matrix;
//print_r($Matrix);
?>