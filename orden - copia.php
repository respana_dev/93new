<?php
	require_once("cache.php");
	require_once("conf.php"); 
	include_once("page_template.html");
	include_once("aplicaciones/dbcon.php");
	
	if(isset($_SESSION['Matrix'])) {
		$Matrix = $_SESSION['Matrix'];
	} else {
		$Matrix = array();
		$_SESSION['Matrix'] = $Matrix;
	}
	
	if(isset($_SESSION['idProv'])) {
		$idProv = $_SESSION['idProv'];
	}
	
	$prv = 0;
	
	function llena_proveedor($idProv) {
		$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
		$sql = "SELECT CodAux, NomAux 
				FROM [SREL].[softland].[cwtauxi] 
				WHERE ClaPro = 'S' AND ClaOtr = 'S' 
				ORDER BY NomAux";
		$rs = odbc_exec($dbcon, $sql);
		while($row = odbc_fetch_array($rs)) {
			if($idProv == $row['CodAux']) { 
				$sel = "selected"; 
			} else { 
				$sel=''; 
			}
			echo '<option value="'.$row['CodAux'].' - '.$row['NomAux'].'" '.$sel.'>'.$row['CodAux'].' - '.$row['NomAux'].'</option>';
		}
	}
	function filtraProv($CodProd,$CodAux) {
		$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
		$sql = "SELECT [CodAux], [codProd], [codProv] 
				FROM [Srel].[dbo].[CodProv] 
				WHERE (codProd = '$CodProd') AND (CodAux = '$CodAux')";
		$rs = odbc_exec($dbcon,$sql);
		$row = odbc_fetch_array($rs);
		return $row['codProd'];
	}
?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<table width="1045" border="0" cellspacing="0" cellpadding="0" align="center" >
							<br>
							<tr>
								<td width="50"></td>
								<td> Proveedor: </td>
								<td colspan="3">
									<form method="GET">
										<select name="selProv" id="selProv" onchange="this.form.submit()" >
											<option value="0">Seleccionar</option>
											<?php llena_proveedor($idProv);?>
										</select>
										(Seleccione el proveedor)
									</form>	
								</td>
							</tr>
						</table>
						<br>
						<?php
							if(isset($_GET["selProv"])){
								$prv = $_GET["selProv"];
							}	
							if ($prv != 0){
								echo '<h4>'.$prv.'</h4>';
								$dg = new C_DataGrid ("SELECT DISTINCT B.CodProd, B.CodSubGru, B.DesProd, B.DesProd2, B.Stock
													   FROM ViewProveedorProd A, ViewStockSrelProd B, CodProv C", "CodProd", "SELECCIONAR");
													   
								$dg -> set_query_filter("(A.CodProd = B.CodProd AND B.CodProd = C.codProd) AND (B.DesProd != '')");	
								
								$dg -> set_theme('aristo');
								$dg -> set_col_width("CodProd", 105);
								$dg -> set_col_width("CodSubGru", 90);
								$dg -> set_col_width("DesProd", 200);
								$dg -> set_col_width("DesProd2", 150);
								$dg -> set_col_width("Stock", 65);
								$dg -> set_col_width("PreUniMB", 100);
								$dg -> set_col_width("Cantidad",100);
								
								$dg -> set_col_edittype("CodSubGru", "select", "AMO:Amortiguadores;CAR:Carroceria;COR:Correas;FIL:Filtros;EMB:Embrague;ENC:Encendido;FRE:Frenos;DIS:Distribucion;MOT:Motor;EMP:Empaquetaduras;REF:Refrigeracion;SUS:Suspension;TRA:Transmision;UNI:Universal;LUB:Lubricantes", false); 
								$dg -> set_conditional_format("Stock","CELL",array("condition"=>"gt", "value"=>"0", "css"=> array("color"=>"#ffffff","background-color"=>"green")));
								$dg -> set_conditional_format("Stock","CELL",array("condition"=>"le", "value"=>"0", "css"=> array("color"=>"red","background-color"=>"#DCDCDC")));
								$dg -> set_locale('es');
								
								$dg -> enable_search(true);
								$dg -> enable_export('excel');
								$dg -> set_col_format('PreUniMB','integer', array('thousandsSeparator'=>',', 'defaultValue'=>'0'));
								$dg -> set_col_format('Stock','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));

								$dg -> enable_debug(true);
								$dg -> set_dimension(1020, 450);
								$dg -> set_multiselect(true, true);
								$dg -> display();
							}
						?>
						<div id="contMain" name="contMain" style="height:1px;width:1px">
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
			$('#page-wrapper').click(function(){
				var rows = getSelRows();
				$("#contMain").load("main.php?cod="+rows);
			});
			function comprar() {
				selProv = document.getElementById("selProv").value;
				if(selProv=='0') {
					alert("Debe seleccionar el proveedor antes de ingresar los items a comprar");
					return;
				}
				window.open("ordenformal.php","cotiza","width=1100,height=500,menubar=no,toolbar=no,location=no");
			}
			function guardaProv(prov) {
				$("#contMain").load("mainorder.php?ac=prov&selProv="+prov.value);
			}
			function guardaCod(row) {
				var e = row.parentNode.parentNode.rowIndex;
				var valor = row.value;
				$("#contMain").load("mainorder.php?cod="+e+"&ac=save&codProv="+valor);
			}
		</script> 
	</body>
</html>