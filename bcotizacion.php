		<?php
			require_once("cache.php");
			include_once("bin/funciones.php");
			include_once("page_template.html");

			$dbcot = odbc_connect('Coti', 'sa', 'SRel9300');
			$dbcon = odbc_connect('Srel', 'sa', 'SRel9300');

			if(isset($_SESSION['Matrix'])) {
				$Matrix = $_SESSION['Matrix'];
			} else {
				$Matrix = array();
				$_SESSION['Matrix'] = $Matrix;
			}

			if(isset($_SESSION['idvend'])) {
				$idVendedor = $_SESSION['idvend'];
			}

			$idRut = $_REQUEST['idRut'];

			if($idRut != '') {
				$idcliente2 = substr($idRut,0,strpos($idRut,"-"));
				$sql = "SELECT * FROM softland.cwtauxi WHERE (CodAux = '$idcliente2')";
				$rs = odbc_exec($dbcon,$sql);
				$row = odbc_fetch_array($rs);
				$cliNom = $row['NomAux'];
			}

			if(isset($_SESSION['dctoGen'])) {
				$dctoGen = $_SESSION['dctoGen'];
			}
		?>
		<form method="post" name="formulario" id="formulario" action="bcotizacion.php">
			<input type="hidden" name="cm" id="cm" value="<?php echo count($Matrix); ?>" />
			<input type="hidden" name="ira" value="1" />
			<div id="page-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<div>
								<table width="1045" border="0" cellspacing="0" cellpadding="0" align="center" >
									<tr>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">&nbsp;</td>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392"><a href="bcotizacion.php">Buscar cotizaciones</a></td>
									</tr>
									<tr>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">&nbsp;</td>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">&nbsp;</td>
									  </tr>
									<tr>
										<td width="120">RUT Cliente</td>
										<td width="10">:</td>
										<td width="392">
											<input type="text" name="idRut" value="<?php echo($idRut); ?>" id="idRut" onkeydown="checkTabPress(this)"/> (con guíon y sin puntos)
										</td>
										<td width="120">Nombre Cliente</td>
										<td width="10">:</td>
										<td width="392"><input name="cliNom" type="text" disabled="disabled" id="cliNom" style="width:250px" onkeydown="checkTabPress(this)" value="<?php echo($cliNom); ?>"/></td>
									</tr>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">&nbsp;</td>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">
											<input type="submit" name="btnBuscar" id="btnBuscar" value="Buscar" />
										</td>
									</tr>
									<tr>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">&nbsp;</td>
										<td width="120">&nbsp;</td>
										<td width="10">&nbsp;</td>
										<td width="392">&nbsp;</td>
									</tr>
								</table>
								<br />
								<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0">
									<tr>
										<td>Cotización Nº</td>
										<td>Vendedor</td>
										<td>Fecha</td>
										<td>Cantidad de items</td>
										<td>Monto Total</td>
									</tr>
									<?php
										if($_POST['ira'] == 1) {
											$sql = "SELECT Cot_general.idCot, Cot_general.fechaCot, Cot_general.vendedorCot, Cot_general.valorCot, count(Cot_detalle.idCot) as Cantidad 
													FROM Cot_general 
													INNER JOIN Cot_detalle ON Cot_general.idCot = Cot_detalle.idCot 
													WHERE Cot_general.clienteCot = '$idRut' 
													GROUP BY Cot_general.idCot, Cot_general.fechaCot, Cot_general.vendedorCot, Cot_general.valorCot, Cot_detalle.idCot";
											$rs = odbc_exec($dbcot,$sql);
											while($row = odbc_fetch_array($rs)) {
												echo '
													<tr onMouseOver="this.style.backgroundColor=\'#DFE0E5\';this.style.cursor=\'hand\'" onMouseOut="this.style.backgroundColor=\'#FFFFFF\'" onClick="cargar(\''.$row[idCot].'\')">
														<td>'.$row[idCot].'</td>
														<td>'.$row[vendedorCot].'</td>
														<td>'.$row[fechaCot].'</td>
														<td>'.$row[Cantidad].'</td>
														<td>'.$row[valorCot].'</td>
													</tr>
												';
											}
										}
									?>
									<tr>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div id="contMain" name="contMain" style="height:100px;width:100%;background-color:#FFF"></div>
	<!-- jQuery -->
		<script src="js/jquery-1.11.0.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script>
			function cargar(valor) {
				$("#contMain").load("bmain.php?coti="+valor);
			}

			function checkTabPress(elemento) {
				if (event.keyCode == 9) {
					nombre=elemento.name;
					valor=elemento.value;
					if(nombre=='idVend') {
						$("#contMain").load("maincart.php?idvend="+valor+"&ac=vend");
					}
					if(nombre=='idRut') {
						$("#contMain").load("maincart.php?idrut="+valor+"&ac=rut");
					}
					if(nombre=='txtCart1') {
						var e = elemento.parentNode.parentNode.rowIndex;
						$("#contMain").load("maincart.php?cod="+e+"&ac=bus&valorbusqueda="+valor);
					}
				}
			}
		</script>
	</body>
</html>