		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
			include_once("aplicaciones/dbcon.php");
		?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">        
                        <?php
							$dg = new C_DataGrid ("SELECT CodProd, CodigoBarra, DesProd2, CodSubGru, DesProd, Stock, PrecioBol, Ubicacion 
												   FROM ViewStockSrelProd", "CodProd", "PRODUCTOS");
							$dg -> set_query_filter("Inactivo='0'");
							$dg -> set_query_filter("DesProd != ''");							
							
							$dg -> set_theme('aristo');
							$dg -> set_col_width("CodProd", 105);
							$dg -> set_col_width("CodigoBarra", 100);
							$dg -> set_col_width("DesProd2", 110);
							$dg -> set_col_width("CodSubGru", 90);
							$dg -> set_col_width("DesProd", 390);
							$dg -> set_col_width("Stock", 70);
							$dg -> set_col_width("PrecioBol", 70);
							
							$dg -> set_col_edittype("CodSubGru", "select", "AMO:Amortiguadores;CAR:Carroceria;COR:Correas;FIL:Filtros;EMB:Embrague;ENC:Encendido;FRE:Frenos;DIS:Distribucion;MOT:Motor;EMP:Empaquetaduras;REF:Refrigeracion;SUS:Suspension;TRA:Transmision;UNI:Universal;LUB:Lubricantes", false); 
							$dg -> set_conditional_format("Stock","CELL",array("condition"=>"gt", "value"=>"0", "css"=> array("color"=>"#ffffff","background-color"=>"green")));
							$dg -> set_conditional_format("Stock","CELL",array("condition"=>"le", "value"=>"0", "css"=> array("color"=>"red","background-color"=>"#DCDCDC")));
							$dg -> set_locale('es');
							
							$dg -> enable_search(true);
							$dg -> enable_export('excel');
							$dg -> set_col_format('PrecioVta','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
							$dg -> set_col_format('PrecioBol','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
							$dg -> set_col_format('Stock','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));

							$dg -> enable_debug(false);
							$dg -> set_dimension(1080, 450);
							$dg -> set_multiselect(true, true);
							$dg -> display();
						?>
						<div id="contMain" name="contMain" style="height:1px;width:1px">
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script type="text/javascript">
			$(function() {
				var grid = jQuery("#PRODUCTOS");
				grid[0].toggleToolbar();
			});
		</script>
	</body>
</html>