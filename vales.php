<?php
require_once("conf.php");   
?>
   


<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SREL930</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">SREL930</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                    <a href="mantenciones.php" ><i class="fa fa-fw fa-wrench"></i>Fichas Mantenciones<b></b></a>
                    
                </li>
                <li class="dropdown">
                    <a href="catalogos.php" ><i class="fa fa-fw fa-wrench"></i>Catalogos<b></b></a>
                    
                </li>
			 <li class="dropdown">
                    <a href="#" ><i class=""></i>Aplicaciones<b></b></a>
                    
                </li>
			             <li class="dropdown">
                    
                  
                    
                </li>
			             <li class="dropdown">
                    <a href="modelo.php"></i> Lista x Marca<b></b></a>
                    
                </li>
                <li class="dropdown">
                    <a href="stock_reponer.php" ><i class=""></i> Reponer Stock<b></b></a>
                   
                </li>
               <li class="dropdown">
                    <a href="devoluciones.php" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-arrows-v"></i>Devoluciones<b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="dev_pendiente.php"><span class="label label-default">Pendiente</span></a>
                        </li>
                       
                        <li>
                            <a href="dev_ok.php"><span class="label label-success">OK</span></a>
                        </li>
                        
                        <li>
                            <a href="dev_despachado.php"><span class="label label-warning">Despachado</span></a>
                        </li>
                        <li>
                            <a href="dev_rechazado.php"><span class="label label-danger">Rechazado</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="devoluciones.php">TODOS</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-table"></i> Pedidos <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="pedidosmsa.php"><i class="fa fa-fw fa-user"></i> MSA</a>
                        </li>
                        <li>
                            <a href="pedidosals.php"><i class="fa fa-fw fa-user"></i> ALS</a>
                        </li>
                        <li>
                            <a href="pedidosref.php"><i class="fa fa-fw fa-user"></i> REFAX</a>
                        </li>
						<li>
                            <a href="pedidosnor.php"><i class="fa fa-fw fa-user"></i> NORIEGA</a>
                        </li>
                         <li>
                            <a href="pedidosgim.php"><i class="fa fa-fw fa-user"></i> GIMPORT</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                            <a href="pedidos.php"><i class="fa fa-fw fa-user"></i> TODOS</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li >
                        <a href="index.php"><i class=""></i> TODOS</a>
                    </li>
                    <li>
                        <a href="nissan.php"><i class=""></i> NISSAN</a>
                    </li>
                    <li>
                        <a href="opel.php"><i class=""></i> OPEL</a>
                    </li>
                    <li>
                        <a href="isuzu.php"><i class=""></i> ISUZU</a>
                    </li>
                      <li>
                        <a href="toyota.php"><i class=""></i> TOYOTA</a>
                    </li>
                    <li>
                       <a href="chevrolet.php"><i class=""></i> CHEVROLET</a>
                    </li>
                    <li>
                       <a href="gmkorea.php"><i class=""></i> GM KOREA</a>
                    </li>
                    <li>
                       <a href="hyundai.php"><i class=""></i> HYUNDAI</a>
                    </li>
					<li>
                        <a href="kia.php"><i class=""></i> KIA</a>
                    </li>
                    <li>
                        <a href="lub.php"><i class=""></i> LUBRICANTES</a>
                    </li>
                    <li>
                        <a href="bat.php"><i class=""></i> BATERIAS</a>
                    </li>
                    <li>
                        <a href="amp.php"><i class=""></i> AMPOLLETAS</a>
                    </li>
                    <li>
                        <a href="correas.php"><i class=""></i> CORREAS</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                     
                           <?php
$dg = new C_DataGrid("SELECT NroVale,VenCod,Glosa1,Fecha,Total FROM softland.vwvales","NroVale","vwvales");


//$dg->enable_kb_nav(true);
$dg->set_locale('es');
$dg -> enable_search(true);
$dg -> set_sortname('NroVale', 'DESC');
//$dg-> setNavOptions('navigator', array ("cloneToTop"=>true) );
$dg->enable_export('EXCEL');
$dg->enable_debug(FALSE);
//segunda grilla valemov
$sdg = new C_DataGrid("SELECT NroVale,Linea,CodProd,CantFacturada,DetProd,TotalBoleta FROM [SREL].[softland].[vwvalemov]", "NroVale", "VWVALES");
$dg-> set_dimension(1000,90);
//$sdg = new C_DataGrid("SELECT NroVale,DetProd FROM softland.vwvalemo", array("NroVale", "vwvalemov"), "vwvalemov");
//$sdg->set_col_title("orderNumber", "Order No.");
//$sdg->set_col_title("productCode", "Product Code");
//$sdg->set_col_title("quantityOrdered", "Quantity");
//$sdg->set_col_title("priceEach", "Unit Price");
//$sdg->set_col_dynalink("productCode", "http://www.example.com/", "orderLineNumber", '&foo');
//$sdg->set_col_format('orderNumber','integer', array('thousandsSeparator'=>'','defaultValue'=>''));
//$sdg->set_col_currency('TotalBoleta','$');

// define master detail relationship by passing the detail grid object as the first parameter, then the foriegn key name.
$dg->set_masterdetail($sdg, 'NroVale');
//$dg->set_subgrid($sdg, 'NroVale');



$dg -> display();
?>
<script type="text/javascript">
$(function() {
var grid = jQuery("#vwvales");
grid[0].toggleToolbar();
});
</script>
</body>
<div class="row">
						 <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Afinamiento</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Motor
                            </li>
                        </ol>
						</div>
                        
                        
                       
                        
                                    
                        
                    </div>
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    


</html>
