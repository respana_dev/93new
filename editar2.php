<?php
require_once("conf.php");
$dg = new C_DataGrid("SELECT [CodProd],[CodBarra], DesProd2, CodSubGr, DesProd, PrecioBol FROM [srel].[softland].[iw_tprod]", "CodProd", "CodProd");
/*
// change column titles
$dg -> set_col_title("orderNumber", "Order No.");
$dg -> set_col_title("orderDate", "Order Date");
$dg -> set_col_title("shippedDate", "Shipped Date");
$dg -> set_col_title("customerNumber", "Customer No.");

// hide a column
$dg -> set_col_hidden("requiredDate");
*/
// enable edit
$dg -> enable_edit("FORM", "CRUD");

$dg -> display();
?>