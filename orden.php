<?php
	require_once("cache.php");
	include_once("page_template.html");
	
	if(isset($_SESSION['Matrix'])) {
		$Matrix = $_SESSION['Matrix'];
	} else {
		$Matrix = array();
		$_SESSION['Matrix'] = $Matrix;
	}

	$pre = null;
	
	if(isset($_SESSION['idProv'])) {
		$idProv = $_SESSION['idProv'];
	}

	function llena_proveedor($idProv) {
		$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
		$sql = "SELECT CodAux, NomAux 
				FROM [SREL].[softland].[cwtauxi] 
				WHERE ClaPro = 'S' AND ClaOtr = 'S' 
				ORDER BY NomAux";
		$rs = odbc_exec($dbcon, $sql);
		while($row = odbc_fetch_array($rs)) {
			if($idProv == $row['CodAux']) { 
				$sel = "selected"; 
			} else { 
				$sel=''; 
			}
			echo '<option value="'.$row['CodAux'].'" '.$sel.'>'.$row['CodAux'].' - '.$row['NomAux'].'</option>';
		}
	}
?>
		<input type="hidden" name="cm" id="cm" value="<?php echo count($Matrix); ?>" />
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div>
							<table width="1045" border="0" cellspacing="0" cellpadding="0" align="center" >
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
								</tr>
								<tr>
									<td width="120"> Proveedor</td>
									<td width="10">:</td>
									<td colspan="4">
										<select name="selProv" id="selProv" onchange="guardaProv(this)">
											<option value="0">Seleccionar</option>
											<?php llena_proveedor($idProv);?>
										</select>            
										(Seleccione el proveedor)
									</td>
								</tr>
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
								</tr>
							</table>
							<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr bgcolor="#FFFFFF">
									<td>
										<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0" id="carrito" name="carrito">
											<tr bgcolor="#CCCCCC">
												<td width="110">Cod Producto</td>
												<td width="110">Cod Proveedor</td>
												<td width="430">Descripción Producto</td>
												<td width="70">Stock</td>
												<td width="70">Precio</td>
												<td width="70">Cantidad</td>
												<td width="75">Total</td>
												<td width="70">Eliminar</td>
											</tr>
											<?php
												$i=0;
												foreach($Matrix as $key=>$row) {
													$sql = "SELECT CodProd, PreUniMB, FechaCompra, DetProd, CodAux 
															FROM Srel.softland.iw_gmovi 
															WHERE (CodProd = '".$row[0]."' AND DetProd IS NOT NULL) AND FechaCompra IS NOT NULL 
															ORDER BY FechaCompra DESC";
													$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
													$rs = odbc_exec($dbcon, $sql);
													while($pre = odbc_fetch_array($rs)) {
														if ($row[0] == $pre['CodProd']){
															$row[4] = $pre['PreUniMB'];
															$row[7] = $row[4]*$row[5];
															echo '
																<tr bgcolor="#FFFFFF">
																	<input type="hidden" name="txtCodigo['.$i.']" id="txtCodigo['.$i.']" value="'.$row[0].'">
																	<td width="105"><label id="txt_1['.$i.']">'.$row[0].'</label></td>
																	<td width="90"><input id="txtCodProv['.$i.']" name="txtCodProv['.$i.']" type="text" style="width:100px" onblur="guardaCod(this)"  value="'.$row[8].'"></td>
																	<td width="390"><label id="txt_3['.$i.']">'.$row[2].'</label></td>
																	<td width="70"><label id="txt_4['.$i.']">'.number_format($row[3]).'</label></td>
																	<td width="70"><input id="txtPrecio['.$i.']" name="txtPrecio['.$i.']" type="text" style="width:50px" onkeyup="valores(this,\'txtPrecio['.$i.']\',\'txtCant['.$i.']\',\'txtTot['.$i.']\',\'txtCodigo['.$i.']\')" value="'.$row[4].'"></td>
																	<td width="70"><input id="txtCant['.$i.']" name="txtCant['.$i.']" type="text" style="width:50px" onkeyup="valores(this,\'txtPrecio['.$i.']\',\'txtCant['.$i.']\',\'txtTot['.$i.']\',\'txtCodigo['.$i.']\')" value="'.$row[5].'"></td>
																	<td width="70"><input id="txtTot['.$i.']" name="txtTot['.$i.']" type="text" style="width:70px" value="'.$row[7].'"></td>
																	<td width="70"><input type="button" style="width:50px" value="X" onclick="borraLinea(this)"></td>
																</tr>
															';
														} else {
															echo '
																<tr bgcolor="#FFFFFF">
																	<input type="hidden" name="txtCodigo['.$i.']" id="txtCodigo['.$i.']" value="'.$row[0].'">
																	<td width="105"><label id="txt_1['.$i.']">'.$row[0].'</label></td>
																	<td width="90"><input id="txtCodProv['.$i.']" name="txtCodProv['.$i.']" type="text" style="width:100px" onblur="guardaCod(this)"  value="'.$row[8].'"></td>
																	<td width="390"><label id="txt_3['.$i.']">'.$row[2].'</label></td>
																	<td width="70"><label id="txt_4['.$i.']">'.number_format($row[3]).'</label></td>
																	<td width="70"><input id="txtPrecio['.$i.']" name="txtPrecio['.$i.']" type="text" style="width:50px" onkeyup="valores(this,\'txtPrecio['.$i.']\',\'txtCant['.$i.']\',\'txtTot['.$i.']\',\'txtCodigo['.$i.']\')" value="'.$row[4].'"></td>
																	<td width="70"><input id="txtCant['.$i.']" name="txtCant['.$i.']" type="text" style="width:50px" onkeyup="valores(this,\'txtPrecio['.$i.']\',\'txtCant['.$i.']\',\'txtTot['.$i.']\',\'txtCodigo['.$i.']\')" value="'.$row[5].'"></td>
																	<td width="70"><input id="txtTot['.$i.']" name="txtTot['.$i.']" type="text" style="width:70px" value="'.$row[7].'"></td>
																	<td width="70"><input type="button" style="width:50px" value="X" onclick="borraLinea(this)"></td>
																</tr>
															';
														}
														break;
													}
													$i++;
													/*
														row 0 = codigo producto interno
														row 1 = codigo sub-grupo
														row 2 = descripcion producto
														row 3 = stock interno 
														row 4 = precio proveedor
														row 5 = cantidad 
														row 6 = 0
														row 7 = valor total 
														row 8 = codigo proveedor 
													*/
												}
											?>
											<tr bgcolor="#FFFFFF">
												<td width="105"><input type="text" id="txtCart1" name="txtCart1" style="width:100px" onkeydown="checkTabPress(this)" /></td> <!-- Codigo producto interno -->
												<td width="110"><input type="text" id="txtCart3" name="txtCart3" style="width:100px" disabled="disabled" /></td> <!-- Codigo proveedor -->
												<td width="430"><input type="text" id="txtCart4" name="txtCart4" style="width:370px" disabled="disabled" /></td> <!-- Descripcion producto -->
												<td width="70"><input type="text" id="txtCart5" name="txtCart5" style="width:50px" disabled="disabled" /></td> <!-- Stock en tienda/bodega -->
												<td><input type="text" id="txtCart6" name="txtCart6" style="width:50px" disabled="disabled" /></td> <!-- Precio en tienda -->
												<td><input type="text" id="txtCart7" name="txtCart7" style="width:50px" disabled="disabled" /></td> <!-- Cantidad a solicitar -->
												<td>&nbsp;</td>
												<td><input type="button" style="width:50px" name="txtBotonC" id="txtBotonC" value=">" onclick="location.reload();" disabled="disabled" /></td>
											</tr>
											<tr bgcolor="#FFFFFF">
												<td width="105">&nbsp;</td>
												<td width="110">&nbsp;</td>
												<td width="430">&nbsp;</td>
												<td width="70">&nbsp;</td>
												<td width="70">&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
											<tr bgcolor="#FFFFFF">
												<td width="105">&nbsp;</td>
												<td width="110">&nbsp;</td>
												<td width="430">
													<input type="hidden" name="TotalReal" id="TotalReal" value="0" />
													<input type="hidden" name="txtDctoG" value="0" id="txtDctoG" />
													&nbsp;
												</td>
												<td width="70"></td>
												<td colspan="2">
													<div align="right">
														TOTAL&nbsp;
													</div>
												</td>
												<td colspan="2">
													<div align="left">
														<input name="txtTotG" type="text" id="txtTotG" style="width:100px" />
													</div>
												</td>
											</tr>
											<tr bgcolor="#FFFFFF">
												<td colspan="10">&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
							<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td align="center">&nbsp;</td>
									<td width="260" align="center">&nbsp;</td>
									<form method="post" action="borraro.php">
										<td width="260" align="center">
											<input type="submit" name="ira2" value="Limpiar Orden" />
										</td>
									</form>
									<td width="260" align="center">
										<input type="submit" name="ira3" value="Comprar" onclick="comprar()" />
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
	<!-- Carrito compra -->
		<div id="contMain" name="contMain" style="height:300px;width:800px;background-color:#FFF">
		</div>
	<!-- jQuery -->
		<script src="js/jquery-1.11.0.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
			function cotizaformal() {
				window.location("carritoformal.php");
				return;
			}

			function valores(row,a,b,d,f) {
				// row corresponde a la fila
				// a = valor
				// b = cantidad
				// c = Descuento
				// d = Total
				// f = código producto
				valP=row.value;
				valF=document.getElementById(f).value;
				var tabla = document.getElementById("carrito");
				tabla.innerHTML;
				if(valF) {
					var lineas = (carrito.rows.length - 5);
					var e = row.parentNode.parentNode.rowIndex;
					valA=a;
					if(isNaN(a)) {
						valA=document.getElementById(a).value;
					}
					valB=document.getElementById(b).value;
					//valC=document.getElementById(c).value;
					if(valB=='') { return; }
					// calculo entre valor y cantidad
					valD=parseFloat(valA)*parseFloat(valB);
				
					// asigna valor en la casilla total de la linea
					document.getElementById(d).value = valD;
					cntTxt = lineas;
					resT=0;
					for(i=0;i<cntTxt;i++) {
						resT=resT+eval(document.getElementById("txtTot["+i+"]").value);
					}
					document.getElementById("txtTotG").value = resT;
					document.getElementById("TotalReal").value = resT;
					// entregamos valores calculados a la Matrix
					valC=0;
					//alert("mainorder.php?cod="+e+"&ac=sum&b="+valB+"&c="+valC+"&d="+valD);
					$("#contMain").load("mainorder.php?cod="+e+"&ac=sum&a="+valA+"&b="+valB+"&c="+valC+"&d="+valD);
					//alert("faf2");
				}
				valoresG('txtDctoG');
				return false;
				//valD=document.getElementById(d).value;
			}

			function borraLinea(row) {
				var e = row.parentNode.parentNode.rowIndex;
				$("#contMain").load("maincart.php?cod="+e+"&ac=del");
				location.reload();
				return false;
			}
			function buscaprod(row) {
				var e = row.parentNode.parentNode.rowIndex;
				var valorbusqueda = document.getElementById('txtCart1').value;
				$("#contMain").load("maincart.php?cod="+e+"&ac=bus&valorbusqueda="+valorbusqueda);
			}
			function valoresG(dctoG) {
				//var e = row.parentNode.parentNode.rowIndex;
				//alert("calcula valores generales");
				valB=document.getElementById('TotalReal').value;
				valC=document.getElementById(dctoG).value;
				if(valC.search("%")!=-1) {
					res=valC.substr(0,valC.search("%"));
					valE=valB-((valB*res)/100);
				} else {
					valE=valB-valC;
				}
				valD=valE;
				document.getElementById('txtTotG').value = valD;
				//$("#contMain").load("maincart.php?idvend="+valor+"&ac=vend");
				$("#contMain").load("maincart.php?ac=dctoG&valdcto="+valC);
			}
			function cotizar() {
				clirut = document.getElementById("idRut").value;
				idvend = document.getElementById("idVend").value;
				if(idvend=='') {
					alert("Debe ingresar el código de vendedor");
					return;
				}
				if(clirut=='') {
					alert("Debe ingresar el RUT del cliente");
					return;
				}
				guardarcliente2();
				window.open("cotiza.php","cotiza","width=400,height=500,menubar=no,toolbar=no,location=no");
			}
			function limpiar() {
				$("#contMain").load("borar.php");
			}
			function comprar() {
				selProv = document.getElementById("selProv").value;
				if(selProv=='0') {
					alert("Debe seleccionar el proveedor antes de ingresar los items a comprar");
					return;
				}
				window.open("ordenformal.php","cotiza","width=1100,height=500,menubar=no,toolbar=no,location=no");
			}
			function guardaProv(prov) {
				$("#contMain").load("mainorder.php?ac=prov&selProv="+prov.value);
			}
			function guardaCod(row) {
				var e = row.parentNode.parentNode.rowIndex;
				var valor = row.value;
				$("#contMain").load("mainorder.php?cod="+e+"&ac=save&codProv="+valor);
			}

			function checkTabPress(elemento) {
				if (event.keyCode == 9) {
					nombre=elemento.name;
					valor=elemento.value;
					if(nombre=='idVend') {
						$("#contMain").load("maincart.php?idvend="+valor+"&ac=vend");
					}
					if(nombre=='idRut') {
						$("#contMain").load("maincart.php?idrut="+valor+"&ac=rut");
					}
					if(nombre=='txtCart1') {
						var e = elemento.parentNode.parentNode.rowIndex;
						$("#contMain").load("mainorder.php?cod="+e+"&ac=bus&valorbusqueda="+valor);
					}
				}
			}
			<?php
				echo "valI=".count($Matrix).";\n";
			?>
			valF=0;
			for(i=0;i<valI;i++) {
				valF=valF+eval(document.getElementById("txtTot["+i+"]").value);
			}
			document.getElementById("txtTotG").value = valF;
			document.getElementById("TotalReal").value = valF;
			<?php
				if($dctoGen!='') {
					echo "valoresG('txtDctoG');";
				}
			?>
			if(document.getElementById('idRut').value) {
				$("#contMain").load("maincart.php?idrut="+document.getElementById('idRut').value+"&ac=rut");
			}
		</script>
	</body>
</html>