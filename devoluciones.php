		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
			include_once("aplicaciones/dbcon.php");
		?>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
							$dg = new C_DataGrID("SELECT ID, COD, DES, UNI, FAL, PRO, GD, NC, EST, TRANS 
												  FROM Srel.dbo.devoluciones","ID", "Devoluciones");

							$dg -> set_col_readonly("ID");
							$dg -> set_col_hidden('ID',FALSE);
							$dg -> set_col_title("COD", "Código");
							$dg -> set_col_title("DES", "Descripción");
							$dg -> set_col_title("UNI", "Unidades");
							$dg -> set_col_title("FAL", "Falla");
							$dg -> set_col_title("PRO", "Proveedor");
							$dg -> set_col_title("GD", "Guía");
							$dg -> set_col_title("NC", "NC");
							$dg -> set_col_title("TRANS", "Transporte");
							$dg -> set_col_title("EST", "Estado");

							$dg -> set_col_edittype('EST', 'select', 'pendiente:Pendiente;despachado:Despachado;ok:OK;rechazado:Rechazado');
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "despachado", "css" => array("color" => "#000000", "background-color" => "orange")));
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "pendiente", "css" => array("color" => "#000000", "background-color" => "yellow")));
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "rechazado", "css" => array("color" => "#000000", "background-color" => "red")));
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "ok", "css" => array("color" => "#000000", "background-color" => "green")));

							$dg -> enable_search(true);
							$dg -> set_theme('aristo');
							$dg -> set_dimension(1080, 420);
							$dg -> enable_edit("FORM", "CRUD");
							$dg -> set_locale('es');
							$dg -> display();
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>