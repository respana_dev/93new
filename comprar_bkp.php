<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
$dbcon2 = odbc_connect('COTI', 'sa', 'SRel9300');

if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}

$idvend = $_SESSION['idvend'];
$idcliente = $_SESSION['idrut'];
$dctoCot = $_SESSION['dctoGen'];

$sql="SELECT max(idCot)+1 AS CodCot FROM Cot_general";
$rs = odbc_exec($dbcon2,$sql);
$row = odbc_fetch_array($rs);
$CodCot = $row['CodCot'];

$sql="SELECT max(NVNumero)+1 AS nv FROM softland.nw_nventa";
$rs = odbc_exec($dbcon,$sql);
$row = odbc_fetch_array($rs);
$NVNumero = $row['nv'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>

<style type="text/css">
.table {
	font-size: 12px;
	font-weight: bold;
	font-family: Tahoma, Geneva, sans-serif;
}
.tahoma11 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
.tahoma12 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
}
.tahoma12negrita {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	font-weight:bold;
}
</style>
</head>

<body>
<table width="400" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><span class="tahoma12negrita">Nota de Venta <?php echo $NVNumero; ?></span></td>
  </tr>
  <tr>
    <td align="center"><span class="tahoma12negrita">Repuestos España</span></td>
  </tr>
  <tr>
    <td align="center">&nbsp;</td>
  </tr>
</table>
<table width="400" border="0" cellpadding="0" cellspacing="0">
<?php
$valorTotal=0;
$valTotalSIVA=0;
$i = 1;
foreach($Matrix as $key=>$row) {
	$valProdSIVA = ceil($row[4]/1.19);
	$valLineaSIVA = $valProdSIVA * $row[5];
	
	$valorTotal=$row[7] + $valorTotal;
	$valorLinea = $row[4] * $row[5];
	
	if($row[6]!=0) {
		$valxl=strpos($row[6],'%');
		if($valxl!=0) {
			$res=substr($row[6],0,$valxl);
			$dctoPtjeLin = $res;
			$dctoValorLin = (($valorLinea*$res)/100);
		} else {
			$dctoPtjeLin = round((($row[6]*100)/$valorLinea),1);
			$dctoValorLin = $row[6];
		}	
		$valDctoSIVA = ceil($dctoValorLin/1.19);
	}
	$valLineaSIVAS = $valLineaSIVA - $valDctoSIVA;
	$sqldet = $sqldet. "INSERT INTO softland.nw_detnv (NVNumero, nvLinea, nvCorrela, nvFecCompr, CodProd, nvCant, nvPrecio, nvEquiv, nvSubTotal, nvDPorcDesc01, nvDDescto01, nvTotDesc, nvTotLinea, DetProd, CheckeoMovporAlarmaVtas, CantUVta) VALUES 
('$NVNumero', '$i', '0', CONVERT (date, GETDATE()), '$row[0]', '$row[5]', '$valProdSIVA', '1', '$valLineaSIVA', '$dctoPtjeLin', '$valDctoSIVA', '$valDctoSIVA', '$valLineaSIVAS', '$row[2]', 'N', '$row[5]')  ";
	//$rs = odbc_exec($dbcon,$sql);	
	$i++;
	echo '
  <tr class="tahoma11">
    <td colspan="5">'.$row[2].'</td>
  </tr>	
  <tr class="tahoma11">
  	<td width="220">'.$row[0].'</td>
    <td width="20">x</td>
    <td width="80">'.$row[5].'</td>
    <td width="20">=</td>
    <td width="80">'.$row[7].'</td>
  </tr>';
  $valTotalSIVA = $valTotalSIVA + $valLineaSIVAS;
}

if($dctoCot!=0) {
	$valx=strpos($dctoCot,'%');
}
if($valx!=0) {
	$res=substr($dctoCot,0,$valx);
	$res2=$valorTotal-(($valorTotal*$res)/100);
	$valE=$res2;
	$dctoPtje = $res;
	$dctoValor = (($valorTotal*$res)/100);
} else {
	$valE=$valorTotal-$dctoCot;
	$dctoPtje = (($dctoCot*100)/$valorTotal);
	$dctoValor = $dctoCot;
}
$valDctoTotalSIVA = ceil($dctoValor/1.19);
$valorSIVA = ($valorTotal / 1.19);
$valorIVA = ($valTotalSIVA * 1.19);

$sql = "INSERT INTO softland.nw_nventa (NVNumero, nvFem, nvEstado, CotNum, NumOC, nvFeEnt, CodAux, VenCod, CodMon, nvObser, NomCon, nvSubTotal, nvPorcDesc01, nvDescto01, nvMonto, nvEquiv, nvNetoAfecto, UsuarioGeneraDocto, FechaHoraCreacion, Sistema, ConcManual, proceso, TotalBoleta) VALUES 
('$NVNumero', CONVERT (date, GETDATE()), 'A', '0', '0', CONVERT (date, GETDATE()), '1', '$idvend', '01', '$idcliente', '', '$valTotalSIVA', '$dctoPtje', '$valDctoTotalSIVA', '$valorIVA', '1', '$valTotalSIVA', 'mespana', CONVERT (date, GETDATE()), 'NW', 'N', 'Notas de Venta', '$valorIVA')";
//echo $sql;
//echo "<br>";
$rs = odbc_exec($dbcon,$sql);
$rs = odbc_exec($dbcon,$sqldet);
//echo $sqldet;
//echo "<br>";
?>
  <tr class="tahoma11">
  	<td width="220"> </td>
    <td width="20"> </td>
    <td width="80"></td>
    <td width="20"></td>
    <td width="80">&nbsp;</td>
  </tr>
  <tr class="tahoma11">
  	<td width="220"> </td>
    <td width="20"> </td>
    <td width="80"></td>
    <td width="20"></td>
    <td width="80">&nbsp;</td>
  </tr>
  <tr class="tahoma11">
  	<td width="220"> </td>
    <td width="20"> </td>
    <td width="80">Total</td>
    <td width="20">&nbsp;</td>
    <td width="80"><?php echo $valorTotal ?></td>
  </tr>
</table>
</body>
</html>
<script>
window.print(false);
</script>