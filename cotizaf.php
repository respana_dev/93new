<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$dbcon2 = odbc_connect('COTI', 'sa', 'SRel9300');
$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}
if(isset($_SESSION['idvend'])) {
	$idVendedor = $_SESSION['idvend'];
}
if(isset($_SESSION['idrut'])) {
	$idRut = $_SESSION['idrut'];
}
if(isset($_SESSION['dctoGen'])) {
	$dctoGen = $_SESSION['dctoGen'];
}
if(isset($_SESSION['cliMarca'])) {
	$cliMarca = $_SESSION['cliMarca'];
} else {
	$cliMarca = $_REQUEST['cliMarca'];
}
if(isset($_SESSION['cliModelo'])) {
	$cliModelo = $_SESSION['cliModelo'];
} else {
	$cliModelo = $_REQUEST['cliModelo'];
}
if(isset($_SESSION['cliAnio'])) {
	$cliAnio = $_SESSION['cliAnio'];
} else {
	$cliAnio = $_REQUEST['cliAnio'];
}
if(isset($_SESSION['cliVIN'])) {
	$cliVIN = $_SESSION['cliVIN'];
} else {
	$cliVIN = $_REQUEST['cliVIN'];
}
if(isset($_SESSION['cliGiro'])) {
	$cliGiro = $_SESSION['cliGiro'];
} else {
	$cliGiro = $_REQUEST['cliGiro'];
}
if(isset($_SESSION['cliFono'])) {
	$cliFono = $_SESSION['cliFono'];
} else {
	$cliFono = $_REQUEST['cliFono'];
}

if(isset($_SESSION['cliNom'])) {
	$cliNom = $_SESSION['cliNom'];
}
if(isset($_SESSION['cliDir'])) {
	$cliDir = $_SESSION['cliDir'];
}
$dctoCot=$_SESSION['dctoGen'];

if(isset($_SESSION['cliComuna'])) {
	$cliComuna = $_SESSION['cliComuna'];
	$sql = "SELECT * FROM Comuna WHERE (comID = $cliComuna)";
	$rs = odbc_exec($dbcon2,$sql);
	$row = odbc_fetch_array($rs);
	$nomComuna = $row['comNombre'];
}

$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

$sql="SELECT max(idCot)+1 AS CodCot FROM Cot_general";
$rs = odbc_exec($dbcon2,$sql);
$row = odbc_fetch_array($rs);
$CodCot = $row['CodCot'];

$sql = "INSERT INTO Cliente VALUES ($CodCot, '$idRut', '$cliMarca', '$cliModelo', '$cliAnio', '$cliVIN', '$cliGiro', '$cliFono')";
// descomentar
$rs = odbc_exec($dbcon2,$sql);

function calculaDcto($valor,$dcto) {
	$xl=strpos($dcto,'%');
	if($xl!=0) {
		$res=substr($dcto,0,$xl);
		$dctoPtje = $res;
		$dctoValor = (($valor*$res)/100);
	} else {
		$dctoPtje = round((($dcto*100)/$valor),1);
		$dctoValor = $dcto;
	}
	return array(ceil($dctoValor),$dctoPtje.'%');
}
?>
<html>
<Head>
<title>Cotizacion</title>
<meta charset="utf-8">
<link href="style.css" rel="stylesheet" type="text/css">
</Head>
<body>
<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="280"><img src="img/logo.jpg" width="220" height="86" /></td>
    <td align="center" class="Texto14negroBold">Sociedad Repuestos España Limitada<br>
      Venta Repuestos y Accesorios Automotrices</td>
    <td width="280" align="right" class="Texto14negroBold"><?php echo "Puerto Montt, ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y');?><br />
    Cotización Nº <?php echo $CodCot; ?></td>
  </tr>
  <tr>
    <td width="280">&nbsp;</td>
    <td align="center" class="Texto12negroBold">RUT: 76.052.614-2</td>
    <td width="280" align="right" class="Texto12negro">Benavente Nº 930, Puerto Montt<br>
      Fono / Fax : 652251596</td>
  </tr>
</table>
<table width="1045" border="0" cellspacing="0" cellpadding="0" align="center" >
  <tr>
    <td class="Texto13negroBold">Marca</td>
    <td class="Texto13negroBold">:</td>
    <td class="Texto13negro"><?php echo($cliMarca); ?></td>
    <td class="Texto13negroBold">Año</td>
    <td class="Texto13negroBold">:</td>
    <td class="Texto13negro"><?php echo($cliAnio); ?></td>
  </tr>
  <tr>
    <td class="Texto13negroBold">Modelo</td>
    <td class="Texto13negroBold">:</td>
    <td class="Texto13negro"><?php echo($cliModelo); ?></td>
    <td class="Texto13negroBold">VIN o Patente</td>
    <td class="Texto13negroBold">:</td>
    <td class="Texto13negro"><?php echo($cliVIN); ?></td>
  </tr>
  <tr>
    <td height="5" class="Texto13negroBold"></td>
    <td height="5" class="Texto13negroBold"></td>
    <td height="5" class="Texto13negro"></td>
    <td height="5" class="Texto13negroBold"></td>
    <td height="5" class="Texto13negroBold"></td>
    <td height="5" class="Texto13negro"></td>
  </tr>
  <tr>
    <td width="120" class="Texto13negroBold">RUT Cliente</td>
    <td width="10" class="Texto13negroBold">:</td>
    <td width="392" class="Texto13negro"><?php echo($idRut); ?></td>
    <td width="120" class="Texto13negroBold">Nombre Cliente</td>
    <td width="10" class="Texto13negroBold">:</td>
    <td width="392" class="Texto13negro"><?php echo($cliNom); ?></td>
  </tr>
  <tr>
    <td width="120" class="Texto13negroBold">Dirección</td>
    <td width="10" class="Texto13negroBold">:</td>
    <td width="392" class="Texto13negro"><?php echo($cliDir); ?></td>
    <td width="120" class="Texto13negroBold">Comuna</td>
    <td width="10" class="Texto13negroBold">:</td>
    <td width="392" class="Texto13negro"><?php echo($nomComuna); ?></td>
  </tr>
  <tr>
    <td class="Texto13negroBold">Giro</td>
    <td class="Texto13negroBold">:</td>
    <td class="Texto13negro"><?php echo($cliGiro); ?></td>
    <td class="Texto13negroBold">Teléfono</td>
    <td class="Texto13negroBold">:</td>
    <td class="Texto13negro"><?php echo $cliFono; ?></td>
  </tr>
  <tr>
    <td width="120">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="392">&nbsp;</td>
    <td width="120">&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td width="392">&nbsp;</td>
  </tr>
</table>
<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0" id="carrito" name="carrito">
  <tr bgcolor="#CCCCCC" class="Texto13negroBold">
    <td width="105">Código</td>
    <td width="110">Categoría</td>
    <td>Descripción Producto</td>
    <td width="70">Valor con IVA</td>
    <td width="70">Cantidad</td>
    <td width="70">Dcto.</td>
    <td width="75">Total</td>
  </tr>
  <?php
$valorTotal=0;
$valTotalSIVA=0;
$i = 1;
foreach($Matrix as $key=>$row) {
	$valorTotal=$row[7] + $valorTotal;
	$sql = "INSERT INTO Cot_detalle VALUES ('$CodCot', '$i','$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]')";
	// descomentar
	$rs = odbc_exec($dbcon2,$sql);	
	echo '
	<tr bgcolor="#FFFFFF"  class="Texto13negro">
            <td width="105"><label id="txt_1['.$i.']">'.$row[0].'</label></td>
            <td width="90"><label id="txt_2['.$i.']">'.$row[1].'</label></td>
            <td><label id="txt_3['.$i.']">'.$row[2].'</label></td>
            <td width="70"><label id="txt_5['.$i.']">$ '.number_format($row[4],0,',','.').'</label></td>
            <td width="70">'.$row[5].'</td>
			<td width="70">$ '.number_format($row[6],0,',','.').'</td>
			<td width="70">$ '.number_format($row[7],0,',','.').'</td>
    </tr>
	';
	$i++;
	$valTotalSIVA = $valTotalSIVA + $valLineaSIVAS;
}
$sql = "INSERT INTO Cot_general (idCot, fechaCot, dctoCot, valorCot, clienteCot, vendedorCot) VALUES('$CodCot',CONVERT (date, GETDATE()),'$dctoCot','$valorTotal','$idcliente','$idvend')";
//descomentar
$rs = odbc_exec($dbcon2,$sql);
?>
  <tr bgcolor="#FFFFFF">
    <td width="105">&nbsp;</td>
    <td width="110">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="75">&nbsp;</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td width="105">&nbsp;</td>
    <td width="110">&nbsp;</td>
    <td>&nbsp;</td>
    <td width="70">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td width="75">&nbsp;</td>
  </tr>
<?php
$descuentoCalculado = calculaDcto($valorTotal, $dctoCot);
$valorTotal = $valorTotal - $descuentoCalculado[0];
$valorNeto = $valorTotal / 1.19;
$valorIVA = $valorTotal - $valorNeto;
?>
  <tr bgcolor="#FFFFFF">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" align="right" class="Texto13negroBold">DESCUENTO</td>
    <td colspan="2" align="right" class="Texto13negroBold"><?php echo '$ '.number_format($descuentoCalculado[0],0,',','.'); ?></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" align="right" class="Texto13negroBold">TOTAL NETO</td>
    <td colspan="2" align="right" class="Texto13negroBold"><?php echo '$ '.number_format($valorNeto,0,',','.'); ?></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="2" class="Texto13negroBold" align="right">IVA</td>
    <td colspan="2"class="Texto13negroBold"><div align="right"><?php echo '$ '.number_format($valorIVA,0,',','.'); ?></div></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td width="105">&nbsp;</td>
    <td width="110">&nbsp;</td>
    <td width="70"></td>
    <td colspan="2" class="Texto13negroBold" align="right">TOTAL&nbsp;</td>
    <td colspan="2"class="Texto13negroBold"><div align="right"><?php echo '$ '.number_format($valorTotal,0,',','.'); ?></div></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="10">&nbsp;</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="10" align="center" class="Texto12negro">Cotización valida por 15 días. Sujeto a disponibilidad de stock.</td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td colspan="10" align="center" class="Texto12negro">Usted fue atendido por el vendedor: <?php echo $idVendedor; ?></td>
  </tr>
</table>
</body>
</html>