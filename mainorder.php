<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

include_once("bin/funciones.php");

$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');

$codigo=$_GET["cod"];
$accion=$_GET["ac"];
$acc = $_POST['acc'];

$valorbusqueda=$_GET["valorbusqueda"];
$val_A=$_GET["a"];
$val_B=$_GET["b"];
$val_C=$_GET["c"];
$val_D=$_GET["d"];

$idVendedor=$_GET["idvend"];
$idRut=$_GET["idrut"];
$valdcto=$_GET["valdcto"];
$idProv=$_GET["selProv"];
$codProv=$_GET["codProv"];

$codigos = array();
$contenido = array();
$codigos = explode(",",$codigo);
$cod=$codigo-1;

if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}
if($accion=="vend") {
	$_SESSION['idvend'] = $idVendedor;
}
if($accion=="prov") {
	$_SESSION['idProv'] = $idProv;
}

if($accion=="del") {
	$cod=$codigo-1;
	del_array($cod);
}
if($accion=="sum") {
	$Matrix[$cod][4]=$val_A;
	$Matrix[$cod][5]=$val_B;
	$Matrix[$cod][6]=$val_C;
	$Matrix[$cod][7]=$val_D;
}
if($accion=="save") {
	$ccod=$Matrix[$cod][0];
	$Matrix[$cod][8] = $codProv;
	$idProv = $_SESSION['idProv'];
	if($codProv!='') {
		$sql = "DELETE FROM CodProv WHERE (codProd = '$ccod') AND (CodAux = '$idProv')";
		$rs = odbc_exec($dbcon,$sql);	
		$sql = "INSERT INTO CodProv VALUES('$idProv','$ccod','$codProv')";
		$rs = odbc_exec($dbcon,$sql);
	}
	$_SESSION['Matrix'] = $Matrix;
}
if($accion=="bus") {
	$cod=$codigo-2;
	if($valorbusqueda!='') { 
		$idProv = $_SESSION['idProv'];
		$sql = "SELECT CodProd, CodSubGru, DesProd, Stock, PrecioBol FROM ViewStockSrelProd WHERE (CodProd = '$valorbusqueda') OR (CodigoBarra = '$valorbusqueda')";
		$rs = odbc_exec($dbcon,$sql);
		$row = odbc_fetch_array($rs);
		$cantidad = count($row);
		//alert($cantidad);
		if($cantidad>2) {
			$codProv=buscaCodProv($row['CodProd'],$idProv);
			echo '
			<script>
			document.getElementById(\'txtBotonC\').disabled = false;
			document.getElementById(\'txtCart1\').value = \''.$row['CodProd'].'\';
			document.getElementById(\'txtCart3\').value = \''.$codProv.'\';
			document.getElementById(\'txtCart4\').value = \''.$row['DesProd'].'\';
			document.getElementById(\'txtCart5\').value = \''.number_format($row['Stock']).'\';
			document.getElementById(\'txtCart6\').value = \''.$row['PrecioBol'].'\';
			document.getElementById(\'txtCart7\').value = \'1\';
			document.getElementById(\'txtCart9\').value = \''.$row['PrecioBol'].'\';
			document.getElementById(\'txtTotG\').value = \'\';
			document.getElementById(\'txtCart3\').disabled = false;
			document.getElementById(\'txtCart4\').disabled = false;
			document.getElementById(\'txtCart5\').disabled = false;
			document.getElementById(\'txtCart6\').disabled = false;
			document.getElementById(\'txtCart7\').disabled = false;
			document.getElementById(\'txtCart8\').disabled = false;
			document.getElementById(\'txtCart9\').disabled = false;
			document.getElementById(\'txtTotG\').disabled = false;
			</script>';
			add_array(array($row['CodProd'],$row['CodSubGru'],$row['DesProd'],$row['Stock'],$row['PrecioBol'],'1','0',$row['PrecioBol'],$codProv),0,$_SESSION["SESS_item"]);
		} else {
			echo '
			<script>
			document.getElementById(\'txtCart1\').value = \'\';
			document.getElementById(\'txtCart3\').value = \'\';
			document.getElementById(\'txtCart4\').value = \'\';
			document.getElementById(\'txtCart5\').value = \'\';
			document.getElementById(\'txtCart6\').value = \'\';
			document.getElementById(\'txtCart8\').value = \'\';
			document.getElementById(\'txtCart9\').value = \'\';
			</script>';	
		}
	}
}
$_SESSION['Matrix']=$Matrix;
?>