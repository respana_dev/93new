<?php
	require_once("cache.php");
	include_once("bin/funciones.php");
	
	$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
	$codi = $_GET["cod"];
	$cds = array();
	$cds = explode(",",$codi);
	
	if (isset($_SESSION['Matrix'])) {
		$Matrix = $_SESSION['Matrix'];
	} else {
		$Matrix = array();
		$_SESSION['Matrix'] = $Matrix;
	}
		
	if (isset($_SESSION["SESS_item"])) {
		$SESS_item = $_SESSION["SESS_item"];
	} else {
		$SESS_item = 1;
		$_SESSION["SESS_item"] = $SESS_item;
	}
	
	if($codi) {
		if(in_array($sub_array[$pos], $cds)) {
			$find = $key;
			return;
		}
		$lcd = end($cds);
		$sql = "SELECT B.ID, A.CodProd, B.COD, A.CodSubGru, A.PrecioBol, A.DesProd, A.Stock 
				FROM Srel.dbo.ViewStockSrelProd2 A, Srel.dbo.pedidos B
				WHERE (A.CodProd = B.CODPROD AND B.ID = '$lcd')
				ORDER BY B.ID DESC";
		$rs = odbc_exec($dbcon, $sql);
		$row = odbc_fetch_array($rs);
		$codProv = buscaCodAux($row['A.CodProd'],$row['B.COD']);
		if(isset($_SESSION['idProv'])){
			$idProv = $_SESSION['idProv'];
		} else {
			$idProv = $codProv;
			$_SESSION['idProv'] = $idProv;
		}
		
		/* 
			$row[0] = código producto
		    $row[1] = código subgrupo
		    $row[2] = detalle producto
		    $row[3] = stock
		    $row[4] = precio compra - precio proveedor
		    $row[5] = cantidad a solicitar 
		    $row[6] = 0
		    $row[7] = valor total - $row[4] * $row[5] 
		    $row[8] = código de proveedor
		*/
		add_array(array($row['A.CodProd'],
					    $row['A.CodSubGru'], 
					    $row['A.DesProd'], 
					    $row['A.Stock'], 
					    intval($row['A.PrecioBol']), 
					    '1', 
					    '0', 
					    intval($row['A.PrecioBol']), 
					    $codProv), 
					    0, 
					    $_SESSION["SESS_item"]);
		$_SESSION["SESS_item"]++;
		$_SESSION["Matrix"] = $Matrix;
	}
?>