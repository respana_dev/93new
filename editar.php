<?php 
	require_once("cache.php");
	require_once("conf.php");
	include_once("page_template.html");
?>
        <div class="row">
            <div class="col-lg-12">
                      <div class="row">
					<ol class="breadcrumb">
						<li></li>
						<li>
							<i></i><a href="editar.php"> EDITAR FICHA PRODUCTO </a>
						</li>
						<li>
							<i></i><a href="editarguias.php"> EDITAR GUIAS DESPACHO </a>
						</li>
						<li>
							<i></i><a href="ubica.php"> EDITAR UBICACIONES </a>
						</li>
					</ol>
				</div>
			</div>
		</div>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
							//realizamos la conexion mediante odbc
							//$cid=odbc_connect($dsn, $usuario, $clave);
							$cid = odbc_connect('SREL', 'sa', 'SRel9300');

							if (!$cid){
								exit("<strong>Ha ocurrido un error tratando de conectarse con la base de datos.</strong>");
							}	

							if (isset($_REQUEST['marca'])) {
								$marca = $_REQUEST['marca'];
							} else {
								$marca = 11;
							}

							$dg = new C_DataGrid ("SELECT CodProd, DesProd, DesProd2, CodBarra, CodSubGr, Inactivo, NivMin, NivRep, NivMax, PrecioVta, PrecioBol 
												   FROM Srel.softland.iw_tprod", "CodProd", "EDITAR_FICHA_PROD");

							$dg -> set_theme('aristo');
							//$dg-> set_col_title("DesProd", "DESCRIPCION");

							$dg -> set_col_width("CodProd", 115);
							$dg -> set_col_width("CodigoBarra", 70);
							$dg -> set_col_width("DesProd2", 120);
							$dg -> set_col_width("CodSubGr", 110);
							$dg -> set_col_width("DesProd", 350);
							$dg -> set_col_width("PrecioBol", 100);
							$dg -> enable_edit("INLINE","CRU");
							$dg -> set_dimension(1080, 400);	
							$dg -> set_col_readonly("CodProd"); 
							$dg -> set_locale('es');
							$dg -> enable_search(true);
							$dg -> enable_export('excel');
							$dg -> set_col_format('PrecioVta','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
							$dg -> set_col_format('PrecioBol','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
							// $dg-> set_col_dynalink("CodProd","http://iis/93/app.php","CodProd");
							$dg -> enable_debug(false);
							$dg -> display();
						?>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>