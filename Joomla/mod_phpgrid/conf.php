<?php
// mysql example
define('PHPGRID_DB_HOSTNAME','localhost'); // database host name
define('PHPGRID_DB_USERNAME', 'root');     // database user name
define('PHPGRID_DB_PASSWORD', ''); // database password
define('PHPGRID_DB_NAME', 'sampledb'); // database name
define('PHPGRID_DB_TYPE', 'mysql');  // database type
define('PHPGRID_DB_CHARSET','utf8'); // ex: utf8(for mysql),AL32UTF8 (for oracle), leave blank to use the default charset

// postgres example
//define('PHPGRID_DB_HOSTNAME','localhost'); // database host name
//define('PHPGRID_DB_USERNAME', 'postgres');     // database user name
//define('PHPGRID_DB_PASSWORD', '1234'); // database password
//define('PHPGRID_DB_NAME', 'sampledb'); // database name
//define('PHPGRID_DB_TYPE', 'postgres');  // database type
//define('PHPGRID_DB_CHARSET','');

// mssql server example
//define('PHPGRID_DB_HOSTNAME','localhost'); // database host name
//define('PHPGRID_DB_USERNAME', 'root');     // database user name
//define('PHPGRID_DB_PASSWORD', ''); // database password
//define('PHPGRID_DB_NAME', 'sampledb'); // database name
//define('PHPGRID_DB_TYPE', 'mssql');  // database type
//define('PHPGRID_DB_CHARSET','');

// oracle server example
//$tnsname = "(DESCRIPTION=(ADDRESS_LIST = (ADDRESS = (PROTOCOL = TCP)(HOST = hosts)(PORT = 3306)))(CONNECT_DATA =(SERVICE_NAME = database)))";
//define('PHPGRID_DB_HOSTNAME',$tnsname); // database host name (TNS)
//define('PHPGRID_DB_USERNAME', 'system');     // database user name
//define('PHPGRID_DB_PASSWORD', '1234'); // database password
//define('PHPGRID_DB_NAME', 'SYSTEM'); // database name
//define('PHPGRID_DB_TYPE', 'oci8');  // database type
//define('PHPGRID_DB_CHARSET','AL32UTF8');

// sqlite server example
//define('PHPGRID_DB_HOSTNAME','c:\path\to\sqlite.db'); // database host name
//define('PHPGRID_DB_USERNAME', '');     // database user name
//define('PHPGRID_DB_PASSWORD', ''); // database password
//define('PHPGRID_DB_NAME', ''); // database name
//define('PHPGRID_DB_TYPE', 'sqlite');  // database type
//define('PHPGRID_DB_CHARSET','');


define('SERVER_ROOT', '/joomla257/modules/mod_phpgrid');

/******** DO NOT MODIFY ***********/
require_once('phpGrid.php');     
/**********************************/
?>
