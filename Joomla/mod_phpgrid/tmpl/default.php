<?php // no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
// require_once("../conf.php");      
require_once($_SERVER["DOCUMENT_ROOT"]. '/joomla257/modules/mod_phpgrid/conf.php');

$dg = new C_DataGrid("SELECT * FROM orders", "orderNumber", "orders");
$dg->enable_debug(false);
$dg -> display(false);
$grid = $dg->get_display(false);
$grid_header = $dg->display_script_includeonce();

$phpgridpath=JURI::root().'modules/mod_phpgrid/';
$document = JFactory::getDocument();
$document->addCustomTag($grid_header);

echo $grid;
?>

