<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');

if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}

$idProv = $_SESSION['idProv'];

$sql="SELECT max(NumOC)+1 AS NumOC FROM softland.owordencom";
$rs = odbc_exec($dbcon,$sql);
$row = odbc_fetch_array($rs);
$NumOC = $row['NumOC'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>

<style type="text/css">
.table {
	font-size: 12px;
	font-weight: bold;
	font-family: Tahoma, Geneva, sans-serif;
}
.tahoma11 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
.tahoma12 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
}
.tahoma12negrita {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	font-weight:bold;
}
.tahoma111 {	font-family: Verdana, Geneva, sans-serif;
	font-size: 11.5px;
}
</style>
</head>

<body>
<table width="300" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><img src="img/logo.jpg" alt="" width="220" height="86" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><span class="tahoma12negrita">Orden de compra <?php echo $NumOC; ?></span></td>
  </tr>
  <tr>
    <td align="center" class="tahoma12negrita">Fecha Emisión: <?php echo date("d-m-Y"); ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="300" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="300" border="0" cellpadding="0" cellspacing="0">
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290">&nbsp;</td>
      </tr>
<?php
$valorTotal=0;
$valTotalSIVA=0;
$i = 1;
foreach($Matrix as $key=>$row) {
	$valProdSIVA = ceil($row[4]/1.19);
	$valLineaSIVA = $valProdSIVA * $row[5];
	$valorTotal=$row[7] + $valorTotal;
	$valorLinea = $row[4] * $row[5];
	$valLineaCIVA = ceil($valorLinea * 1.19);
	$valLineaSIVAS = $valLineaSIVA - $valDctoSIVA;
	$sqldet = $sqldet . "INSERT INTO softland.owordendet (NumInterOC, NumLinea, CodProd, FechaEnt, Cantidad, Recibido, Saldo, PrecioUnit, PrecioUnitMB, ValorTotal, ValorTotalMB, DetProd) VALUES ($NumOC, $i, '$row[0]', CONVERT (date, GETDATE()), '$row[5]', '0', '$row[5]', '$row[4]', '$row[4]', '$valLineaCIVA', '$valLineaCIVA', '$row[2]')";
	$i++;
	echo '
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290">'.substr($row[2],0,39).'</td>
      </tr>
      <tr class="tahoma111">
        <td>&nbsp;</td>
        <td>
		<table width="290" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><strong>'.$row[0].'</strong></td>
            <td width="20"><strong>'.$row[5].'</strong></td>
			<td width="10"><strong>x</strong></td>
            <td align="right"><strong>$ '.number_format($row[4],0,',','.').'&nbsp;</strong></td>
            <td width="10" align="center"><strong>=</strong></td>
            <td width="80" align="right"><strong>$ '.number_format($row[7],0,',','.').'&nbsp;</strong></td>
          </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
';
  $valTotalSIVA = $valTotalSIVA + $valLineaSIVAS;
}
$valorIVA = ceil($valorTotal * 1.19);

$sql = "INSERT INTO softland.owordencom (NumInterOC, NumOC, FechaOC, CodEstado, CodAux, FecFinalOC, CodConP, CodMon, EquivMonOC, SubTotalOC, SubTotalMB, NetoAfecto, NetoAfectoMB, ValorTotOC, ValorTotMB) VALUES 
('$NumOC', '$NumOC', CONVERT (date, GETDATE()), 'AP', '$idProv', CONVERT (date, GETDATE()), '30', '01', '1', '$valorTotal', '$valorTotal', '$valorTotal', '$valorTotal', '$valorIVA', '$valorIVA')  ";

// descomentar las siguientes dos lineas
$rs = odbc_exec($dbcon,$sql);
$rs = odbc_exec($dbcon,$sqldet);

?>
    </table></td>
  </tr>
</table>
<table width="300" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="300" border="0" cellpadding="0" cellspacing="0">
      <tr class="tahoma111">
        <td width="10"></td>
        <td width="290"><table width="290" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><strong>Total&nbsp;</strong></td>
            <td width="10"><strong>=</strong></td>
            <td width="100" align="right"><strong><?php echo '&nbsp;$ '.number_format($valorTotal,0,',','.'); ?></strong>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290"></td>
      </tr>
    </table></td>
  </tr>
</table>

<br />
<!--
<0A0A0A0A0A0A0A0A0A0A0A0A>
-->
</body>
</html>