<?php 
	require_once("cache.php");
	require_once("conf.php");
	include_once("page_template.html");
?>
        <div class="row">
            <div class="col-lg-12">
                      <div class="row">
					<ol class="breadcrumb">
						<li></li>
						<li>
							<i></i><a href="editar.php"> EDITAR FICHA PRODUCTO </a>
						</li>
						<li>
							<i></i><a href="editarguias.php"> EDITAR GUIAS DESPACHO </a>
						</li>
						<li>
							<i></i><a href="ubica.php"> EDITAR UBICACIONES </a>
						</li>
					</ol>
				</div>
			</div>
		</div>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
						 // realizamos la conexion mediante odbc
						 // $cid = odbc_connect($dsn, $usuario, $clave);
							$cid = odbc_connect('SREL', 'sa', 'SRel9300');

							if (!$cid){
								exit("<strong>Ha ocurrido un error tratando de conectarse con la base de datos.</strong>");
							}	

							if (isset($_REQUEST['marca'])) {
								$marca = $_REQUEST['marca'];
							} else {
								$marca = 11;
							}

							$dg = new C_DataGrid ("SELECT CodProd, UbicProd 
												   FROM Srel.softland.iw_nivst", "CodProd", "EDITAR_UBICACIONES");

							$dg -> set_theme('aristo');

							$dg -> enable_edit("INLINE","CRU");
							$dg -> set_dimension(800, 370);	
							$dg -> set_col_readonly("CodProd"); 

							$dg -> set_locale('es');
							$dg -> enable_search(true);
							$dg -> enable_export('excel');
							$dg -> enable_debug(false);
							$dg -> display();
						?>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>