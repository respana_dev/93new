		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
			include_once("aplicaciones/dbcon.php");
		?>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
						 // realizamos la conexion mediante odbc
						 // $cid = odbc_connect($dsn, $usuario, $clave);
							$cid = odbc_connect('SREL', 'sa', 'SRel9300');

							if (!$cid){
								exit("<strong>Ha ocurrido un error tratando de conectarse con la base de datos.</strong>");
							}	

							if (isset($_REQUEST['marca'])) {
								$marca = $_REQUEST['marca'];
							} else {
								$marca = 11;
							}

							$dg = new C_DataGrid ("SELECT * FROM pedidos", "ID", "PEDIDOS");
							$dg -> set_query_filter("PRO LIKE '%REF%'");

							$dg -> set_col_readonly("ID");
							$dg -> set_col_hidden('ID', FALSE);
							$dg -> set_col_hidden('VALOR');
							$dg -> set_col_hidden('MESON');
							$dg -> set_col_hidden('UBICACION');

							$dg -> set_col_title("DesSubGr", "Sub Grupo");
							$dg -> set_col_title("CODPROD", "Código");
							$dg -> set_col_title("COD", "Cod Proveedor");
							$dg -> set_col_title("DES", "Descripción");
							$dg -> set_col_title("UNI", "Unidades");
							$dg -> set_col_title("MOD", "Modelo");
							$dg -> set_col_title("PRO", "Nom Proveedor");
							$dg -> set_col_title("EST", "Estado");
							$dg -> set_col_title("DIA", "Fecha");
							$dg -> set_col_title("VEN", "Vendedor");

							$dg -> enable_search(true);
							$dg -> set_sortname('ID', 'DESC');
							$dg -> set_col_width("DES", 390);
							$dg -> set_dimension(1020,420);                                                        

							$dg -> set_col_edittype('EST', 'select', 'pendiente:Vta_Directa;cotizar:Cotizar;pedido:Pedido;reposicion:Reposicion;agotado:Agotado;recibido:Recibido');

							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "pedido", "css" => array("color" => "#ffffff", "background-color" => "green")));
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "cotizar", "css" => array("color" => "#ffffff", "background-color" => "blue")));    
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "pendiente", "css" => array("color" => "#000000", "background-color" => "yellow")));
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "agotado", "css" => array("color" => "#000000", "background-color" => "red")));
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "recibido", "css" => array("color" => "#000000", "background-color" => "orange")));  
							$dg -> set_conditional_format("EST", "CELL", array("condition" => "eq", "value" => "reposicion", "css" => array("color" => "#000000", "background-color" => "purple")));   

							$dg -> set_theme('aristo');
							$dg -> enable_edit("FORM","CRUD");
							$dg -> enable_kb_nav(true);
							$dg -> set_locale('es');
							$dg -> enable_export('excel');
							$dg -> enable_debug(FALSE);
							$dg -> display();
						?>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>