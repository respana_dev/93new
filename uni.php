
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>ACCESORIOS</title>
</head>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>

<body>
<?php include_once("header.php"); ?>


<div id="wrapper">
 <?php
  require_once("conf.php");

$dg = new C_DataGrid ("SELECT CodProd, CodigoBarra, DesProd2, CodSubGru, DesProd, Stock, PrecioBol, Ubicacion FROM ViewStockSrelProd", "CodProd","ACCESORIOS");
//$dg-> set_query_filter("DesProd LIKE '%BATERIA%'");
//$dg-> set_query_filter("CodGrupo = '4'");
$dg-> set_query_filter("CodSubGru= 'UNI'");
//$dg-> set_col_hidden("Inactivo");

//$dg->enable_edit("INLINE","CRUD"); 

$dg -> set_theme('aristo');
//$dg-> set_col_title("DesProd", "DESCRIPCION");
$dg-> set_col_width("CodProd", 105);
$dg-> set_col_width("CodigoBarra", 100);
$dg-> set_col_width("DesProd2", 110);
$dg-> set_col_width("CodSubGru", 90);
$dg-> set_col_width("DesProd", 390);
$dg-> set_col_width("Stock", 70);
$dg-> set_col_width("PrecioBol", 70);
//$dg-> set_col_edittype ("DesProd","textarea");
//$dg->enable_edit("FORM","CRUD");
	
//$dg -> set_col_default('Impuesto', '-1');
//$dg->set_col_default('CodUMed','01');
//$dg -> set_col_hidden("Impuesto");
//$dg -> set_col_hidden("CodUMed");
// $dg -> set_col_readonly("Impuesto"); 
$dg -> set_col_edittype("CodSubGru", "select", "AMO:Amortiguadores;CAR:Carroceria;COR:Correas;FIL:Filtros;EMB:Embrague;ENC:Encendido;FRE:Frenos;DIS:Distribucion;MOT:Motor;EMP:Empaquetaduras;REF:Refrigeracion;SUS:Suspension;TRA:Transmision;UNI:Universal;LUB:Lubricantes", false); 
//$dg-> set_col_currency("PrecioVta","$","",",",2,"0");
//$dg-> set_col_currency("PrecioBol","$","",",",2,"0");
//$dg->enable_kb_nav(true);
$dg->set_conditional_format("Stock","CELL",array("condition"=>"gt",
                                                      "value"=>"0",
                                                      "css"=> array("color"=>"#ffffff","background-color"=>"green")));

$dg->set_conditional_format("Stock","CELL",array("condition"=>"le",
                                                  "value"=>"0",
                                                  "css"=> array("color"=>"red","background-color"=>"#DCDCDC")));

$dg->set_locale('es');
$dg -> enable_search(true);
$dg -> enable_export('excel');
//$dg->enable_advanced_search(true);
$dg -> set_col_format('PrecioVta','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
$dg -> set_col_format('PrecioBol','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
$dg -> set_col_format('Stock','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
$dg-> set_col_dynalink("CodProd","http://iis/91/app.php","CodProd");
$dg->enable_debug(false);
$dg -> set_dimension(1020, 450);
$dg -> display();?>
<div id="headerwrap">
<table width="10%" border="0" align="" cellpadding="0" cellspacing="0">
  <tr class="">

    <td width="10" id="header"><table width="0%" border="0" cellspacing="0" cellpadding="0">
        
       
        <tr>
          
        </tr>
        
      </table></td>
    <td width="400">&nbsp;</td>
    <td id="header" width="300px"><table width="" border="0" cellspacing="0" cellpadding="0">
       
        
    <td width="300">&nbsp;</td>
    <td width="300" align="" id="header"><table width="" border="0" cellspacing="0" cellpadding="0">
      <tr>
       


?>
      </tr>
     
      <tr>
       
      </td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
    <td width="0" align="" id="header"><table width="0" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
      </tr>
       
     <tr>
        
      </tr>
    </table></td>
   
  </tr>
</table>

 
</div>
</div>

</body>
</html>