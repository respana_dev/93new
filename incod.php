		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
			include_once("aplicaciones/dbcon.php");
		?>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
							$dg = new C_DataGrid("SELECT CodGrupo, CodSubGru, CodProd, CodigoBarra, DesProd2,  DesProd, Stock, PrecioBol, Ubicacion 
												  FROM ViewStockSrelProd", "CodProd", "PRODUCTOS");
												  
							$dg -> set_theme('aristo');

							$dg -> set_col_width("CodProd", 105);
							$dg -> set_col_width("CodigoBarra", 100);
							$dg -> set_col_width("DesProd2", 110);
							$dg -> set_col_width("CodSubGru", 90);
							$dg -> set_col_width("DesProd", 390);
							$dg -> set_col_width("Stock", 70);
							$dg -> set_col_width("PrecioBol", 70);

							$dg -> set_col_title("CodSubGru", "Sub grupo");
							$dg -> set_col_title("CodGrupo", "Marca");
							$dg -> set_col_title("CodigoBarra", "Cod Barra");
							$dg -> set_col_title("CodProd", "Código");
							$dg -> set_col_title("DesProd2", "OEM");
							$dg -> set_col_title("DesProd", "Descripción");
							$dg -> set_col_title("PrecioBol", "Precio");

							$dg -> set_dimension(1020, 450);
							$dg -> set_col_edittype("CodSubGru", "select", "AMO:Amortiguadores;CAR:Carroceria;COR:Correas;FIL:Filtros;EMB:Embrague;ENC:Encendido;FRE:Frenos;DIS:Distribucion;MOT:Motor;EMP:Empaquetaduras;REF:Refrigeracion;SUS:Suspension;TRA:Transmision;UNI:Universal;LUB:Lubricantes", false);
							$dg -> set_col_edittype("CodGrupo", "select", "11:NISSAN;12:OPEL;13:ISUZU;14:GM KOREA;15:TOYOTA;16:CHEVROLET;86:HYUNDAI;87:KIA;UNI:UNIVERSALES;NS11:NISSAN ORIGINAL;T15:TOYOTA ORIGINAL", false);

							$dg -> set_locale('es');
							$dg -> enable_search(true);
							$dg -> enable_export('excel');
							$dg -> set_sortname('CodProd', 'DESC');

							$dg -> set_col_format('PrecioVta', 'integer', array('thousandsSeparator' => '.', 'defaultValue' => '0'));
							$dg -> set_col_format('PrecioBol', 'integer', array('thousandsSeparator' => '.', 'defaultValue' => '0'));
							$dg -> set_col_format('Stock', 'integer', array('thousandsSeparator' => '.', 'defaultValue' => '0'));

							$dg -> enable_debug(false);
							$dg -> display();
						?>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>