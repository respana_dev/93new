			<?php
				require_once("cache.php");
				require_once("conf.php"); 
				include_once("page_template.html");
				include_once("aplicaciones/dbcon.php");
			?>
			<div id="page-wrapper">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-12">
							<?php
								$dg = new C_DataGrid("SELECT * FROM dbo.mantenciones", "id", "MANTENCIONES");

								$dg -> set_col_link("url"); 
								$dg -> set_col_hidden("id");
								$dg -> set_col_readonly("id, url");
								$dg -> set_col_width("marca", 40);
								$dg -> enable_edit("INLINE", "CRUD");
								$dg -> enable_search(true);
								$dg -> set_theme('aristo');
								$dg -> set_dimension(1020, 425);
								$dg -> display();
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>