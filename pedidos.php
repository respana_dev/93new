		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
			include_once("aplicaciones/dbcon.php");
			
		?>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
							$dg = new C_DataGrid ("SELECT ID, CODPROD, COD, DES, UNI, MOD, PRO, EST, DIA, VEN, VALOR
												   FROM pedidos", 
												   "ID", "PEDIDOS");
														
							$dg -> set_theme('aristo');
							$dg -> set_locale('es');
														
							$dg -> set_col_readonly("ID");
							$dg -> set_col_hidden('ID',FALSE);
							
							$dg -> set_col_title("CODPROD", "COD PROD");
							$dg -> set_col_title("COD", "COD PROV");
							$dg -> set_col_title("DES", "DESCRIPCION");
							$dg -> set_col_title("UNI", "UNIDAD");
							$dg -> set_col_title("MOD", "MODELO");
							$dg -> set_col_title("PRO", "PROVEEDOR");
							$dg -> set_col_title("EST", "ESTADO");
							$dg -> set_col_title("DIA", "FECHA PEDIDO");
							$dg -> set_col_title("VEN", "VENDEDOR");
							$dg -> set_col_title("VALOR", "COSTO");
							
							$dg -> set_col_width("DES", 390);
							$dg -> set_col_width("CODPROD", 150);
							$dg -> set_col_width("COD", 150);
							$dg -> set_col_width("UNI", 120);
							$dg -> set_col_width("DIA", 200);
							$dg -> set_col_width("EST", 180);
							
							$dg -> enable_export('excel');
							$dg -> enable_search(true);
							$dg -> set_col_format("VALOR", "integer", array("thousandsSeparator" => ".", "defaultValue" => "0"));                                                           
							$dg -> set_col_edittype('EST', 'select', 'pendiente:Vta_Directa;cotizar:Cotizar;pedido:Pedido;reposicion:Reposicion;agotado:Agotado;recibido:Recibido;urgente:Urgente');

							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "pedido","css" => array("color" => "#ffffff","background-color" => "green")));
							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "cotizar","css" => array("color" => "#ffffff","background-color" => "blue")));    
							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "pendiente","css" => array("color" => "#000000","background-color" => "yellow")));
							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "agotado","css" => array("color" => "#000000","background-color" => "red")));
							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "recibido","css" => array("color" => "#000000","background-color" => "orange")));  
							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "reposicion","css" => array("color" => "#000000","background-color" => "purple"))); 
							$dg -> set_conditional_format("EST","CELL",array("condition" => "eq","value" => "urgente","css" => array("color" => "#000000","background-color" => "cyan")));

							$dg -> enable_edit("FORM","CRUD");
							$dg -> set_pagesize(100);
							$dg -> set_sortname('ID', 'DESC');
							$dg -> enable_debug(false);
							$dg -> set_dimension(1080, 420);
							$dg -> display();
						?>
						<div id="contMain" name="contMain" style="height:1px;width:1px">
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
			$('#page-wrapper').click(function(){
				var rows = getSelRows();
				$("#contMain").load("main.php?cod="+rows);
			});
		</script>
	</body>
</html>