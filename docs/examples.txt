If you are new to phpGrid, we recommend that you go through the examples in the sequence provided for each section. 

In addition to the examples, major phpGrid features can be seen in Screenshots page. phpGrid utilizes jQuery extensively. Currently phpGrid supports jQuery 1.3.2. If you are already using jQuery but a different version, considering to remove the existing jQuery to avoid version conflict. If you have questions regarding this, please send us an email.

Please note, certain features, such as update, delete, and add new, are disabled in live examples.

--- Basic methods ----

Example 1: A Basic PHP Datagrid

The basic PHP datagrid requires only as little as TWO lines of code. Foremostly, always create phpGrid object in the first line; then call its methods to define properties, and lastly always call display() to render to screen. 

Please note in some databases, such as Firebird, MS Access, the fields name are case-sensitive. Make sure the name used in the code matches the case when you are using those type of databases.

// parameter 1: SQL statement
// parameter 2: SQL primary key
// parameter 3: SQl table name
$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders");
$dg -> display();   

See it live!


Example 2: Descriptive Column Headers

By default phpGrid pulls out the data field name defined in database as the header for each column. 
You can simply change the title using method set_col_title().

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
$dg -> display();

See it live!


Example 3: Hide a Datagrid Column

For columns need not to be shown, such as primary key, use method set_col_hidden() to hide those columns.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");
 
$dg -> display();

See it live!


Example 4: Data grid Caption

By default, phpGrid displays table name as the caption. It can be changed using set_caption(). You can also hide the caption by using an empty string "" as the caption.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");
 
$dg -> display();

See it live!


Example 5: Export datagrid to Excel or HTML 

phpGrid currently supports export in native Excel format and HTML format. When the export feature is enabled, phpGrid displays the export icon in the footer. 

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");

// set export type
$dg -> enable_export('EXCEL');
 
$dg -> display();

See it live!


Example: 6 Integrated Search

phpGrid includes integrated search. By default, it is not enabled. To enable search use enable_search() method with parameter set to true. Once enabled, the integrated search can be toggled with the search button located in the footer.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");

// set export type
$dg -> enable_export('EXCEL');

// enable integrated search
$dg -> enable_search(true);
 
$dg -> display();
 

Example: 7 Height and Width 

set_dimension() method specifies data grid initial height and width. The default height and width is 400 and 300 pixels.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");

// set export type
$dg -> enable_export('EXCEL');

// enable integrated search
$dg -> enable_search(true);

// set height and weight of datagrid
$dg -> set_dimension(800, 600); 
 
$dg -> display();
 

Example: 9 Datagrid Pagination Size

By default, the pagination size is 20. It means maximum of 20 records can be dipslayed in a single page. This value can be easily changed with set_pagesize() method. Be aware that pagination is disabled when set_scroll() is set to true. See the next example on set_scroll() for more information.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");

// set export type
$dg -> enable_export('EXCEL');

// enable integrated search
$dg -> enable_search(true);

// set height and weight of datagrid
$dg -> set_dimension(800, 600); 

// increase pagination size to 40 from default 20
$dg -> set_pagesize(40);
 
$dg -> display();

 
Example: 10 Use veritcal scroll to load data

In contrast to traditional pagination method to browse through data, you can use vertical scroll as alternative way to load data. It is done by set parameter in set_scroll() to true. When scroll position changes, phpGrid makes ajax call in the background and refresh the content in the grid.It does not load everything at once and only hold all the items from the start through to the latest point ever visited. This prevents memory leaks from happening. 

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");

// set export type
$dg -> enable_export('EXCEL');

// enable integrated search
$dg -> enable_search(true);

// set height and weight of datagrid
$dg -> set_dimension(800, 600); 

// increase pagination size to 40 from default 20
// $dg -> set_pagesize(40);

// use vertical scroll to load data
$dg -> set_scroll(true);
 
$dg -> display();

 
Example: 11 Theme Roller

The overall look and feel of the phpGrid can be easily changed using set_theme() method. The theme elements are completely customizable through CSS. Theme files, css and images, are stored in css folder.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// change default caption
$dg -> set_captin("Orders List");

// set export type
$dg -> enable_export('EXCEL');

// enable integrated search
$dg -> enable_search(true);

// set height and weight of datagrid
$dg -> set_dimension(800, 600); 

// increase pagination size to 40 from default 20
// $dg -> set_pagesize(40);

// use vertical scroll to load data
$dg -> set_scroll(true);

// change theme
$dg -> set_theme('flick');
 
$dg -> display();


Example: 12 Display Url as Hyperlink 

Display the text in column as static hyperlink. Note this method only display static links stored in database field. To display dynamic links based on variables, see the next example on set_col_dynalink() method. 

Please note we are using Orders table in this example. 

$dg2= new C_DataGrid("select * from Products", "productCode", "ProductCode");
$dg2->set_col_title("productCode", "Product Code");
$dg2->set_col_title("productName", "Product Name");
$dg2->set_col_title("productLine", "Product Line");

// display static Url
$dg2->set_col_link("vendorUrl");                                             
                                                                                     
$dg2->display();


Example: 13 Display Dynamic Url 

It's nice that you can easily display static Url using set_col_link(). However, it is often for database drive webpage that Url is dynamically formed based on parameters value passed. Method set_col_dynalink() is designed specially for this purpose. The following example demostrates passing the column orderNumber as the dynamic value used in Url.

$dg2= new C_DataGrid("select * from Products", "productCode", "ProductCode");
$dg2->set_col_title("productCode", "Product Code");
$dg2->set_col_title("productName", "Product Name");
$dg2->set_col_title("productLine", "Product Line");
          
// display static Url
$dg2->set_col_link("productUrl"); 
                  
// display dynamic url. e.g.http://www.example.com/?productCode=101&foo=bar
$dg2 -> set_col_dynalink("productCode", "http://www.example.com/", "productCode", '&foo=bar');                                                                   
$dg2->display();


Example: 14 Text Format

phpGrid comes with several frequently used data formatter through set_col_format() method. Different data format has different format options. In most cases, users only need to set column formatting for "integer", "number", and "mail" using this method. For "currency',"link", and "showlink" formats, phpGrid provides a number of helper functions to make formatting simpler and easier. Refer to <a href="/docs/">documentations</a> on column formatter helper methods.

$dg2= new C_DataGrid("select * from Products", "productCode", "ProductCode");
$dg2->set_col_title("productCode", "Product Code");
$dg2->set_col_title("productName", "Product Name");
$dg2->set_col_title("productLine", "Product Line");

// display static Url
$dg2->set_col_link("productUrl");                                             

// dynamic url. e.g.http://www.example.com/?productCode=101&foo=bar
$dg2 -> set_col_dynalink("productCode", "http://www.example.com/", "productCode", '&foo=bar');

// column format
$dg2->set_col_currency("buyPrice");
$dg2->set_col_format("quantityInStock", "integer", array("thousandsSeparator" => ","));   
$dg2->set_col_format("MSRP", 'currency', array("prefix" => "$",
                                                "suffix" => '',
                                                "thousandsSeparator" => ",",
                                                "decimalSeparator" => ".",
                                                "decimalPlaces" => '2',
                                                "defaultValue" => '0.00'));

// the above line is equivalent to the following helper function                        
$dg2->set_col_currency("MSRP", "$", '', ",",".", "2", "0.00");                                                                                       
$dg2->display();
       


Example: 15 Image Display

Display image is easy. Simply set the column name in set_col_img(), phpGrid will take care of the rest.

$dg2= new C_DataGrid("select * from Products", "productCode", "ProductCode");
$dg2->set_col_title("productCode", "Product Code");
$dg2->set_col_title("productName", "Product Name");
$dg2->set_col_title("productLine", "Product Line");

// display static Url
$dg2->set_col_link("productUrl");                                             

// dynamic url. e.g.http://www.example.com/?productCode=101&foo=bar
$dg2 -> set_col_dynalink("productCode", "http://www.example.com/", "productCode", '&foo=bar');

// column format
$dg2->set_col_currency("buyPrice");
$dg2->set_col_format("quantityInStock", "integer", array("thousandsSeparator" => ","));   
$dg2->set_col_format("MSRP", 'currency', array("prefix" => "$",
                                                "suffix" => '',
                                                "thousandsSeparator" => ",",
                                                "decimalSeparator" => ".",
                                                "decimalPlaces" => '2',
                                                "defaultValue" => '0.00'));

// the above line is equivalent to the following helper function                        
$dg2->set_col_currency("MSRP", "$", '', ",",".", "2", "0.00");                                      

// display image
$dg6->set_col_img("Image");
                                                 
$dg2->display();


Example: 16 Resize PHP datagrid by dragging the mouse

When enabled, the small triangle icon is displayed at the right bottom corner of the php data grid. The grid can be resized by simply click and drag the icon using mouse. This is more convenient than setting the width and height in the code.

This function is currently in beta. 

$dg2= new C_DataGrid("select * from Products", "productCode", "ProductCode");
$dg2->set_col_title("productCode", "Product Code");
$dg2->set_col_title("productName", "Product Name");
$dg2->set_col_title("productLine", "Product Line");

// display static Url
$dg2->set_col_link("productUrl");                                             

// dynamic url. e.g.http://www.example.com/?productCode=101&foo=bar
$dg2 -> set_col_dynalink("productCode", "http://www.example.com/", "productCode", '&foo=bar');

// column format
$dg2->set_col_currency("buyPrice");
$dg2->set_col_format("quantityInStock", "integer", array("thousandsSeparator" => ","));   
$dg2->set_col_format("MSRP", 'currency', array("prefix" => "$",
                                                "suffix" => '',
                                                "thousandsSeparator" => ",",
                                                "decimalSeparator" => ".",
                                                "decimalPlaces" => '2',
                                                "defaultValue" => '0.00'));

// the above line is equivalent to the following helper function                        
$dg2->set_col_currency("MSRP", "$", '', ",",".", "2", "0.00");                                      

// display image
$dg6->set_col_img("Image");

// enable resize by dragging mouse
$dg6 -> enable_resize(); 
                                                 
$dg2->display();




---- CRUD Example --------

Example 17: Edit Datagrid

The PHP datagrid is not editable by default. You can enable edit by calling enable_edit(). Currently, two types of edit modes are supported, FORM edit and INLINE. When Form edit mode is used, additional icons appear in the data grid footer used for editing.  When set to inline mode, the cells in the selected row become editable. Only a single row can be edited for either mode.

When edit is enabled, all of the CRUD operations, Create, Read, Update, and Delete, are supported by default. You can restrict types of edit operations permitted by specifying the second parameter in enable_edit(). For example, to disallow Delete, change the $operations='CRU'.  

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// enable edit
$dg->enable_edit("INLINE", "CRUD"); 

$dg->display();


Example 18: Read only fields

Sometimes we don't want certain fields to be editable, then we can use set_col_readonly() methods. Note that you can set more than one columns to be read only using this method.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");

// enable edit
$dg->enable_edit("INLINE", "CRUD"); 
 
// hide a column
$dg -> set_col_hidden("requiredDate");

// read only columns, one or more columns delimited by comma
$dg -> set_col_readonly("orderDate, customerNumber"); 
 
$dg -> display();

See it live! Click on row to see the fields that are set to read only.


Example 19: Required fields

To set fields as required during edit, you can use set_col_required() method. If required fields are left blank, an "* required" message will be displayed. One more more columns can be specified as required fields in this method.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// enable edit
$dg->enable_edit("INLINE", "CRUD"); 

// hide a column
$dg -> set_col_hidden("requiredDate");

// read only columns, one or more columns delimited by comma
$dg -> set_col_readonly("orderDate, customerNumber"); 

// required fields
$dg -> set_col_required("orderNumber, customerNumber");
 
$dg -> display();

See it live! Click on row to to toggle inline edit and leave the required fields blank


Example 20: Select Multiple Records

You can select multiple records with set_multiselect() method. When multiselect is enabled, a checkbox is shown to the left of each row. It is currently designed to delete multiple records. More functionalities will be added to multiselect in the future.
 
$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// enable edit
$dg->enable_edit("INLINE", "CRUD"); 

// hide a column
$dg -> set_col_hidden("requiredDate");

// read only columns, one or more columns delimited by comma
$dg -> set_col_readonly("orderDate, customerNumber"); 

// required fields
$dg -> set_col_required("orderNumber, customerNumber");

// multiple select
$dg -> set_multiselect(true);
 
$dg -> display();


Example 21: Set HTML Controls Used for Editing

After enabling edit, you can set the type of HTML control for any editable column used.  List of Available controls are: text, textarea, select, checkbox, and password. Text and textarea are set automatically based on the database access library <a href="http://phplens.com/lens/adodb/docs-adodb.htm#metatype">ADOdb metatype</a> as the default edit control. 
    
$dg2 = new C_DataGrid("select * from employees", "employeeNumber", "Employees");
$dg2->set_col_title("employeeNumber", "Emp No.");
$dg2->set_col_title("lastName", "Last Name");
$dg2->set_col_title("firstName", "First Name");
$dg2->set_col_title("isActive", "Active?");
$dg2->set_col_format("email", "email");
$dg2->enable_edit("FORM", "CRUD");   
$dg2->set_row_color("","","#DDEEF5"); 
$dg2->set_col_hidden('employeeNumber',false);

$dg2->set_col_edittype("isActive", "checkbox","1:0");
$dg2->set_col_edittype("officeCode", "select", "1:San Francisco;2:Boston;3:NYC;4:Paris;5:Tokyo;6:Sydney;7:London");    

$dg2->display();

See it live!


Example 22: Master detail

The phpGrid went extra miles to make creating the master detail datagrid a super simple task.  There is essentially a simple function to set_masterdetail() to set the master detail relationship between two datagrids, and have them work interactively. The detail grid is a regular phpGrid object and can have the same methods like any other datagrid, such as description title, sort, and update etc. A master grid can have one detail datagrid.  The detail grid renders dynamically based on the row selected in parent grid. 

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// enable edit
$dg->enable_edit("INLINE", "CRUD"); 

// hide a column
$dg -> set_col_hidden("requiredDate");

// read only columns, one or more columns delimited by comma
$dg -> set_col_readonly("orderDate, customerNumber"); 

// required fields
$dg -> set_col_required("orderNumber, customerNumber");

// multiple select
$dg -> set_multiselect(true);
 
// second grid as detail grid. Notice it is just another regular phpGrid with properites.
$sdg = new C_DataGrid("SELECT orderNumber,productCode,quantityOrdered,priceEach FROM OrderDetails", "orderNumber", "OrderDetails");
$sdg->set_col_title("orderNumber", "Order No.");
$sdg->set_col_title("productCode", "Product Code");
$sdg->set_col_title("quantityOrdered", "Quantity");
$sdg->set_col_title("priceEach", "Unit Price");
$sdg->set_col_dynalink("productCode", "http://www.example.com/", "orderLineNumber", '&foo');
$sdg->set_col_format('orderNumber','integer', array('thousandsSeparator'=>'','defaultValue'=>''));
$sdg->set_col_currency('priceEach','$');

// define master detail relationship by passing the detail grid object as the first parameter, then the foriegn key name.
$dg->set_masterdetail($sdg, 'orderNumber');

$dg->display();

See it live!

Example 23: Subgrid

Defining a subgrid is essentially the same as setting up the master detail grid as described in our previous example. Again, only a single line of code change is required by calling set_subgrid(). The difference between a master detail datagrid and a subgrid is that the subgrid displays simple read-only inline grid rather than in a separate datagrid. 

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", "Orders"); 

// change column titles
$dg->set_col_title("orderNumber", "Order No.");
$dg->set_col_title("orderDate", "Order Date");
$dg->set_col_title("shippedDate", "Shipped Date");
$dg->set_col_title("customerNumber", "Customer No.");
 
// enable edit
$dg->enable_edit("INLINE", "CRUD"); 

// hide a column
$dg -> set_col_hidden("requiredDate");

// read only columns, one or more columns delimited by comma
$dg -> set_col_readonly("orderDate, customerNumber"); 

// required fields
$dg -> set_col_required("orderNumber, customerNumber");

// multiple select
$dg -> set_multiselect(true);
 
// second grid as detail grid. Notice it is just another regular phpGrid with properites.
$sdg = new C_DataGrid("SELECT orderNumber,productCode,quantityOrdered,priceEach FROM OrderDetails", "orderNumber", "OrderDetails");
$sdg->set_col_title("orderNumber", "Order No.");
$sdg->set_col_title("productCode", "Product Code");
$sdg->set_col_title("quantityOrdered", "Quantity");
$sdg->set_col_title("priceEach", "Unit Price");

// define master detail relationship by passing the detail grid object as the first parameter, then the foriegn key name.
$dg->set_subgrid($sdg, 'orderNumber');

$dg->display();


Example 24: Large data set

phpGrid is capable of loading and rendering large data set quickly and efficiently. The number of rows is about half million for this example. 


---- Complex Datagrid Example ----

Example 25: Multiple Editable PHP datagrids

Did you know it is possible to have multiple editable grids in a single page? No need to separate your data management into multiple pages. You can now performa all your content administrative tasks in one page.

$dg = new C_DataGrid("SELECT * FROM Orders", "orderNumber", 'Orders');
$dg->display();

$dg2 = new C_DataGrid("select * from employees", "employeeNumber", "Employees");
$dg2->display();

$dg3 = new C_DataGrid("select * from offices", "officeCode", "Offices");
$dg3->display();

$dg4 = new C_DataGrid("select * from productlines", "productLine", "ProductLines");
$dg4->display();

$dg5 = new C_DataGrid("select * from customers", "customerNumber", "Customers");
$dg5->display();

See it yourself!

Would you like to see more examples? Let us know! 


VERSION 4.3

Multiple Databases 

phpGrid nows supports data source from more than one database on a single page. Simply use the 4th parameter in constructor (C_DataGrid) to overwrite the initial database settings in conf.php. For example, grid A binds a table from one database, while grid B binds another table from a completely different database, all on the same page.


<?php
$gridA = new C_DataGrid("SELECT * FROM orders", "orderNumber", "orders");
$gridA->enable_edit("INLINE","CRUD");
$gridA -> display();

// overwrite database settings with another database.
$gridB = new C_DataGrid("SELECT * FROM names", "id", "names",
						array("hostname"=>"localhost",
							"username"=>"root",
							"password"=>"",
							"dbname"=>"utf8db", 
							"dbtype"=>"mysql", 
							"dbcharset"=>"utf8"));

$gridB->enable_edit("INLINE","CRUD"); 
$gridB -> display();
?>

Non-English Characters Support

If database is created in charset other than default charset, e.g. "latin1", some characters may be displayed as "?" in the grid. To properly display non-English characters, such as Spanish, Fresh, or Chinese, you must define the value of 'PHPGRID_DB_CHARSET' variable in conf.php.

define('PHPGRID_DB_HOSTNAME','localhost'); // database host name
define('PHPGRID_DB_USERNAME', 'root');     // database user name
define('PHPGRID_DB_PASSWORD', ''); // database password
define('PHPGRID_DB_NAME', 'sampledb'); // database name
define('PHPGRID_DB_TYPE', 'mysql');  // database type
define('PHPGRID_DB_CHARSET','utf8'); // ex: utf8(for mysql), AL32UTF8(for oracle), leave blank to use the default charset


Locale Settings

If you are using phpGrid in a language rather English, you may want the datagrid to display appropriate system text used by phpGrid such as "Submit", "Next", "Page", "Find" etc. The language locale files are located in js\src\i18n directory.  

$dg = new C_DataGrid("SELECT * FROM orders", "orderNumber", "orders");
$dg->set_locale('fr');        
$dg->enable_edit('FORM');

$dg -> display();  



