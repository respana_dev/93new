<?php
	require_once("cache.php");
	include_once("page_template.html");
	include_once("aplicaciones/dbcon.php");
	
	if(isset($_SESSION['Matrix'])) {
		$Matrix = $_SESSION['Matrix'];
	} else {
		$Matrix = array();
		$_SESSION['Matrix'] = $Matrix;
	}
	if(isset($_SESSION['idvend'])) {
		$idVendedor = $_SESSION['idvend'];
	}
	if(isset($_SESSION['idrut'])) {
		$idRut = $_SESSION['idrut'];
	}
	if(isset($_SESSION['dctoGen'])) {
		$dctoGen = $_SESSION['dctoGen'];
	}
	function calculaDcto($valor,$dcto) {
		$xl=strpos($dcto,'%');
		if($xl!=0) {
			$res=substr($dcto,0,$xl);
			$dctoPtje = $res;
			$dctoValor = (($valor*$res)/100);
		} else {
			$dctoPtje = round((($dcto*100)/$valor),1);
			$dctoValor = $dcto;
		}
		return array(ceil($dctoValor),$dctoPtje.'%');
	}
?>
		<input type="hidden" name="cm" id="cm" value="<?php echo count($Matrix); ?>" />
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<div>
							<table width="1045" border="0" cellspacing="0" cellpadding="0" align="center" >
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392"><a href="bcotizacion.php">Buscar cotizaciones</a></td>
								</tr>
								<tr>
									<td width="120">Código Vendendor</td>
									<td width="10">:</td>
									<td width="392"><input type="text" name="idVend" value="<?php echo($idVendedor); ?>" id="idVend" onkeydown="checkTabPress(this)"/></td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
								</tr>
								<tr>
									<td width="120">RUT Cliente</td>
									<td width="10">:</td>
									<td width="392"><input type="text" name="idRut" value="<?php echo($idRut); ?>" id="idRut" onkeydown="checkTabPress(this)"/> 
									(con guíon y sin puntos)</td>
									<td width="120">Nombre Cliente</td>
									<td width="10">:</td>
									<td width="392"><input type="text" name="cliNom" value="<?php echo($cliNom); ?>" id="cliNom" onkeydown="checkTabPress(this)" style="width:250px"/></td>
								</tr>
								<tr>
									<td width="120">Dirección</td>
									<td width="10">:</td>
									<td width="392"><input type="text" name="cliDir" value="<?php echo($cliDir); ?>" id="cliDir" onkeydown="checkTabPress(this)" style="width:250px"/></td>
									<td width="120">Comuna</td>
									<td width="10">:</td>
									<td width="392">
										<select name="cliComuna" id="cliComuna">
											<option value="0">Seleccionar</option>
											<option value="15101">Arica</option>
											<option value="15102">Camarones</option>
											<option value="15201">Putre</option>
											<option value="15202">General Lagos</option>
											<option value="1101">Iquique</option>
											<option value="1402">Camiña</option>
											<option value="1403">Colchane</option>
											<option value="1404">Huara</option>
											<option value="1405">Pica</option>
											<option value="1401">Pozo Almonte</option>
											<option value="1107">Alto Hospicio</option>
											<option value="2101">Antofagasta</option>
											<option value="2102">Mejillones</option>
											<option value="2103">Sierra Gorda</option>
											<option value="2104">Taltal</option>
											<option value="2201">Calama</option>
											<option value="2202">Ollagüe</option>
											<option value="2203">San Pedro de Atacama</option>
											<option value="2301">Tocopilla</option>
											<option value="2302">María Elena</option>
											<option value="3101">Copiapó</option>
											<option value="3102">Caldera</option>
											<option value="3103">Tierra Amarilla</option>
											<option value="3201">Chañaral</option>
											<option value="3202">Diego de Almagro</option>
											<option value="3301">Vallenar</option>
											<option value="3302">Alto del Carmen</option>
											<option value="3303">Freirina</option>
											<option value="3304">Huasco</option>
											<option value="4101">La Serena</option>
											<option value="4102">Coquimbo</option>
											<option value="4103">Andacollo</option>
											<option value="4104">La Higuera</option>
											<option value="4105">Paiguano</option>
											<option value="4106">Vicuña</option>
											<option value="4201">Illapel</option>
											<option value="4202">Canela</option>
											<option value="4203">Los Vilos</option>
											<option value="4204">Salamanca</option>
											<option value="4301">Ovalle</option>
											<option value="4302">Combarbalá</option>
											<option value="4303">Monte Patria</option>
											<option value="4304">Punitaqui</option>
											<option value="4305">Río Hurtado</option>
											<option value="5101">Valparaíso</option>
											<option value="5102">Casablanca</option>
											<option value="5103">Concón</option>
											<option value="5104">Juan Fernández</option>
											<option value="5105">Puchuncaví</option>
											<option value="5801">Quilpué</option>
											<option value="5107">Quintero</option>
											<option value="5804">Villa Alemana</option>
											<option value="5109">Viña del Mar</option>
											<option value="5201">Isla  de Pascua</option>
											<option value="5301">Los Andes</option>
											<option value="5302">Calle Larga</option>
											<option value="5303">Rinconada</option>
											<option value="5304">San Esteban</option>
											<option value="5401">La Ligua</option>
											<option value="5402">Cabildo</option>
											<option value="5403">Papudo</option>
											<option value="5404">Petorca</option>
											<option value="5405">Zapallar</option>
											<option value="5501">Quillota</option>
											<option value="5502">Calera</option>
											<option value="5503">Hijuelas</option>
											<option value="5504">La Cruz</option>
											<option value="5802">Limache</option>
											<option value="5506">Nogales</option>
											<option value="5803">Olmué</option>
											<option value="5601">San Antonio</option>
											<option value="5602">Algarrobo</option>
											<option value="5603">Cartagena</option>
											<option value="5604">El Quisco</option>
											<option value="5605">El Tabo</option>
											<option value="5606">Santo Domingo</option>
											<option value="5701">San Felipe</option>
											<option value="5702">Catemu</option>
											<option value="5703">Llaillay</option>
											<option value="5704">Panquehue</option>
											<option value="5705">Putaendo</option>
											<option value="5706">Santa María</option>
											<option value="6101">Rancagua</option>
											<option value="6102">Codegua</option>
											<option value="6103">Coinco</option>
											<option value="6104">Coltauco</option>
											<option value="6105">Doñihue</option>
											<option value="6106">Graneros</option>
											<option value="6107">Las Cabras</option>
											<option value="6108">Machalí</option>
											<option value="6109">Malloa</option>
											<option value="6110">Mostazal</option>
											<option value="6111">Olivar</option>
											<option value="6112">Peumo</option>
											<option value="6113">Pichidegua</option>
											<option value="6114">Quinta de Tilcoco</option>
											<option value="6115">Rengo</option>
											<option value="6116">Requínoa</option>
											<option value="6117">San Vicente</option>
											<option value="6201">Pichilemu</option>
											<option value="6202">La Estrella</option>
											<option value="6203">Litueche</option>
											<option value="6204">Marchihue</option>
											<option value="6205">Navidad</option>
											<option value="6206">Paredones</option>
											<option value="6301">San Fernando</option>
											<option value="6302">Chépica</option>
											<option value="6303">Chimbarongo</option>
											<option value="6304">Lolol</option>
											<option value="6305">Nancagua</option>
											<option value="6306">Palmilla</option>
											<option value="6307">Peralillo</option>
											<option value="6308">Placilla</option>
											<option value="6309">Pumanque</option>
											<option value="6310">Santa Cruz</option>
											<option value="7101">Talca</option>
											<option value="7102">Constitución</option>
											<option value="7103">Curepto</option>
											<option value="7104">Empedrado</option>
											<option value="7105">Maule</option>
											<option value="7106">Pelarco</option>
											<option value="7107">Pencahue</option>
											<option value="7108">Río Claro</option>
											<option value="7109">San Clemente</option>
											<option value="7110">San Rafael</option>
											<option value="7201">Cauquenes</option>
											<option value="7202">Chanco</option>
											<option value="7203">Pelluhue</option>
											<option value="7301">Curicó</option>
											<option value="7302">Hualañé</option>
											<option value="7303">Licantén</option>
											<option value="7304">Molina</option>
											<option value="7305">Rauco</option>
											<option value="7306">Romeral</option>
											<option value="7307">Sagrada Familia</option>
											<option value="7308">Teno</option>
											<option value="7309">Vichuquén</option>
											<option value="7401">Linares</option>
											<option value="7402">Colbún</option>
											<option value="7403">Longaví</option>
											<option value="7404">Parral</option>
											<option value="7405">Retiro</option>
											<option value="7406">San Javier</option>
											<option value="7407">Villa Alegre</option>
											<option value="7408">Yerbas Buenas</option>
											<option value="8101">Concepción</option>
											<option value="8102">Coronel</option>
											<option value="8103">Chiguayante</option>
											<option value="8104">Florida</option>
											<option value="8105">Hualqui</option>
											<option value="8106">Lota</option>
											<option value="8107">Penco</option>
											<option value="8108">San Pedro de la Paz</option>
											<option value="8109">Santa Juana</option>
											<option value="8110">Talcahuano</option>
											<option value="8111">Tomé</option>
											<option value="8112">Hualpén</option>
											<option value="8201">Lebu</option>
											<option value="8202">Arauco</option>
											<option value="8203">Cañete</option>
											<option value="8204">Contulmo</option>
											<option value="8205">Curanilahue</option>
											<option value="8206">Los Álamos</option>
											<option value="8207">Tirúa</option>
											<option value="8301">Los Ángeles</option>
											<option value="8302">Antuco</option>
											<option value="8303">Cabrero</option>
											<option value="8304">Laja</option>
											<option value="8305">Mulchén</option>
											<option value="8306">Nacimiento</option>
											<option value="8307">Negrete</option>
											<option value="8308">Quilaco</option>
											<option value="8309">Quilleco</option>
											<option value="8310">San Rosendo</option>
											<option value="8311">Santa Bárbara</option>
											<option value="8312">Tucapel</option>
											<option value="8313">Yumbel</option>
											<option value="8314">Alto Biobío</option>
											<option value="8401">Chillán</option>
											<option value="8402">Bulnes</option>
											<option value="8403">Cobquecura</option>
											<option value="8404">Coelemu</option>
											<option value="8405">Coihueco</option>
											<option value="8406">Chillán Viejo</option>
											<option value="8407">El Carmen</option>
											<option value="8408">Ninhue</option>
											<option value="8409">Ñiquén</option>
											<option value="8410">Pemuco</option>
											<option value="8411">Pinto</option>
											<option value="8412">Portezuelo</option>
											<option value="8413">Quillón</option>
											<option value="8414">Quirihue</option>
											<option value="8415">Ránquil</option>
											<option value="8416">San Carlos</option>
											<option value="8417">San Fabián</option>
											<option value="8418">San Ignacio</option>
											<option value="8419">San Nicolás</option>
											<option value="8420">Treguaco</option>
											<option value="8421">Yungay</option>
											<option value="9101">Temuco</option>
											<option value="9102">Carahue</option>
											<option value="9103">Cunco</option>
											<option value="9104">Curarrehue</option>
											<option value="9105">Freire</option>
											<option value="9106">Galvarino</option>
											<option value="9107">Gorbea</option>
											<option value="9108">Lautaro</option>
											<option value="9109">Loncoche</option>
											<option value="9110">Melipeuco</option>
											<option value="9111">Nueva Imperial</option>
											<option value="9112">Padre Las Casas</option>
											<option value="9113">Perquenco</option>
											<option value="9114">Pitrufquén</option>
											<option value="9115">Pucón</option>
											<option value="9116">Saavedra</option>
											<option value="9117">Teodoro Schmidt</option>
											<option value="9118">Toltén</option>
											<option value="9119">Vilcún</option>
											<option value="9120">Villarrica</option>
											<option value="9121">Cholchol</option>
											<option value="9201">Angol</option>
											<option value="9202">Collipulli</option>
											<option value="9203">Curacautín</option>
											<option value="9204">Ercilla</option>
											<option value="9205">Lonquimay</option>
											<option value="9206">Los Sauces</option>
											<option value="9207">Lumaco</option>
											<option value="9208">Purén</option>
											<option value="9209">Renaico</option>
											<option value="9210">Traiguén</option>
											<option value="9211">Victoria</option>
											<option value="14101">Valdivia</option>
											<option value="14102">Corral</option>
											<option value="14202">Futrono</option>
											<option value="14201">La Unión</option>
											<option value="14203">Lago Ranco</option>
											<option value="14103">Lanco</option>
											<option value="14104">Los Lagos</option>
											<option value="14105">Máfil</option>
											<option value="14106">Mariquina</option>
											<option value="14107">Paillaco</option>
											<option value="14108">Panguipulli</option>
											<option value="14204">Río Bueno</option>
											<option value="10101">Puerto Montt</option>
											<option value="10102">Calbuco</option>
											<option value="10103">Cochamó</option>
											<option value="10104">Fresia</option>
											<option value="10105">Frutillar</option>
											<option value="10106">Los Muermos</option>
											<option value="10107">Llanquihue</option>
											<option value="10108">Maullín</option>
											<option value="10109">Puerto Varas</option>
											<option value="10201">Castro</option>
											<option value="10202">Ancud</option>
											<option value="10203">Chonchi</option>
											<option value="10204">Curaco de Vélez</option>
											<option value="10205">Dalcahue</option>
											<option value="10206">Puqueldón</option>
											<option value="10207">Queilén</option>
											<option value="10208">Quellón</option>
											<option value="10209">Quemchi</option>
											<option value="10210">Quinchao</option>
											<option value="10301">Osorno</option>
											<option value="10302">Puerto Octay</option>
											<option value="10303">Purranque</option>
											<option value="10304">Puyehue</option>
											<option value="10305">Río Negro</option>
											<option value="10306">San Juan de la Costa</option>
											<option value="10307">San Pablo</option>
											<option value="10401">Chaitén</option>
											<option value="10402">Futaleufú</option>
											<option value="10403">Hualaihué</option>
											<option value="10404">Palena</option>
											<option value="11101">Coihaique</option>
											<option value="11102">Lago Verde</option>
											<option value="11201">Aisén</option>
											<option value="11202">Cisnes</option>
											<option value="11203">Guaitecas</option>
											<option value="11301">Cochrane</option>
											<option value="11302">O'Higgins</option>
											<option value="11303">Tortel</option>
											<option value="11401">Chile Chico</option>
											<option value="11402">Río Ibáñez</option>
											<option value="12101">Punta Arenas</option>
											<option value="12102">Laguna Blanca</option>
											<option value="12103">Río Verde</option>
											<option value="12104">San Gregorio</option>
											<option value="12201">Cabo de Hornos</option>
											<option value="12202">Antártica</option>
											<option value="12301">Porvenir</option>
											<option value="12302">Primavera</option>
											<option value="12303">Timaukel</option>
											<option value="12401">Natales</option>
											<option value="12402">Torres del Paine</option>
											<option value="13101">Santiago</option>
											<option value="13102">Cerrillos</option>
											<option value="13103">Cerro Navia</option>
											<option value="13104">Conchalí</option>
											<option value="13105">El Bosque</option>
											<option value="13106">Estación Central</option>
											<option value="13107">Huechuraba</option>
											<option value="13108">Independencia</option>
											<option value="13109">La Cisterna</option>
											<option value="13110">La Florida</option>
											<option value="13111">La Granja</option>
											<option value="13112">La Pintana</option>
											<option value="13113">La Reina</option>
											<option value="13114">Las Condes</option>
											<option value="13115">Lo Barnechea</option>
											<option value="13116">Lo Espejo</option>
											<option value="13117">Lo Prado</option>
											<option value="13118">Macul</option>
											<option value="13119">Maipú</option>
											<option value="13120">Ñuñoa</option>
											<option value="13121">Pedro Aguirre Cerda</option>
											<option value="13122">Peñalolén</option>
											<option value="13123">Providencia</option>
											<option value="13124">Pudahuel</option>
											<option value="13125">Quilicura</option>
											<option value="13126">Quinta Normal</option>
											<option value="13127">Recoleta</option>
											<option value="13128">Renca</option>
											<option value="13129">San Joaquín</option>
											<option value="13130">San Miguel</option>
											<option value="13131">San Ramón</option>
											<option value="13132">Vitacura</option>
											<option value="13201">Puente Alto</option>
											<option value="13202">Pirque</option>
											<option value="13203">San José de Maipo</option>
											<option value="13301">Colina</option>
											<option value="13302">Lampa</option>
											<option value="13303">Tiltil</option>
											<option value="13401">San Bernardo</option>
											<option value="13402">Buin</option>
											<option value="13403">Calera de Tango</option>
											<option value="13404">Paine</option>
											<option value="13501">Melipilla</option>
											<option value="13502">Alhué</option>
											<option value="13503">Curacaví</option>
											<option value="13504">María Pinto</option>
											<option value="13505">San Pedro</option>
											<option value="13601">Talagante</option>
											<option value="13602">El Monte</option>
											<option value="13603">Isla de Maipo</option>
											<option value="13604">Padre Hurtado</option>
											<option value="13605">Peñaflor</option>
											<option value="99999">Ignorada</option>
										</select>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392"><input type="submit" name="ira" value="Guardar Cliente" onclick="guardarcliente()" /></td>
								</tr>
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
								</tr>
							</table>
							<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0" id="carrito" name="carrito">
								<tr bgcolor="#CCCCCC">
									<td width="105">CodProd</td>
									<td width="110">CodSubGru</td>
									<td width="430">DesProd</td>
									<td width="70">Stock</td>
									<td width="70">PrecioBol</td>
									<td width="70">Cantidad</td>
									<td width="70">Dcto.</td>
									<td width="75">Total</td>
									<td width="70">Eliminar</td>
								</tr>
								<?php 
									$i=0;
									foreach($Matrix as $key => $row) {
										echo '
										<input type="hidden" name="txtPrecio['.$i.']" id="txtPrecio['.$i.']" value="'.$row['PrecioBol'].'">
										<input type="hidden" name="txtCodigo['.$i.']" id="txtCodigo['.$i.']" value="'.$row[0].'">
										<tr bgcolor="#FFFFFF">
												<td width="105"><label id="txt_1['.$i.']">'.$row[0].'</label></td>
												<td width="90"><label id="txt_2['.$i.']">'.$row[1].'</label></td>
												<td width="390"><label id="txt_3['.$i.']">'.$row[2].'</label></td>
												<td width="70"><label id="txt_4['.$i.']">'.number_format($row[3]).'</label></td>
												<td width="70"><label id="txt_5['.$i.']">$ '.number_format($row[4]).'</label></td>
												<td width="70"><input id="txtCant['.$i.']" name="txtCant['.$i.']" type="text" style="width:50px" onkeyup="valores(this,'.$row[4].',\'txtCant['.$i.']\',\'txtDesc['.$i.']\',\'txtTot['.$i.']\',\'txtCodigo['.$i.']\')" value="'.$row[5].'"></td>
												<td width="70"><input id="txtDesc['.$i.']" name="txtDesc['.$i.']" type="text" style="width:50px" onkeyup="valores(this,'.$row[4].',\'txtCant['.$i.']\',\'txtDesc['.$i.']\',\'txtTot['.$i.']\',\'txtCodigo['.$i.']\')" value="'.$row[6].'"></td>
												<td width="70"><input id="txtTot['.$i.']" name="txtTot['.$i.']" type="text" style="width:70px" value="'.$row[7].'"></td>
												<td width="70"><input type="button" style="width:50px" value="X" onclick="borraLinea(this)"></td>
										</tr>
										';
										$i++;
									}
								?>      
								<tr bgcolor="#FFFFFF">
									<td width="105"><input type="text" id="txtCart1" name="txtCart1" style="width:100px" onkeydown="checkTabPress(this)" /></td>
									<td width="110"><input type="text" id="txtCart3" name="txtCart3" style="width:80px" disabled="disabled"></td>
									<td width="430"><input type="text" id="txtCart4" name="txtCart4" style="width:370px" disabled="disabled"></td>
									<td width="70"><input type="text" id="txtCart5" name="txtCart5" style="width:50px" disabled="disabled"></td>
									<td><input type="text" id="txtCart6" name="txtCart6" style="width:50px" disabled="disabled"></td>
									<td><input type="text" id="txtCart7" name="txtCart7" style="width:50px" onKeyUp="valores(this,'txtCart6','txtCart7','txtCart8','txtCart9','txtCart1')" disabled="disabled"></td>
									<td><input type="text" id="txtCart8" name="txtCart8" style="width:50px" onKeyUp="valores(this,'txtCart6','txtCart7','txtCart8','txtCart9','txtCart1')" disabled="disabled"></td>
									<td width="75"><input type="text" id="txtCart9" name="txtCart9" style="width:70px" disabled="disabled"></td>
									<td><input type="button" style="width:50px" name="txtBotonC" id="txtBotonC" value=">" onclick="location.reload();" disabled="disabled"></td>
								</tr>
								<tr bgcolor="#FFFFFF">
									<td width="105">&nbsp;</td>
									<td width="110">&nbsp;</td>
									<td width="430">&nbsp;</td>
									<td width="70">&nbsp;</td>
									<td width="70">&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td width="75">&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr bgcolor="#FFFFFF">
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td width="430">&nbsp;</td>
									<td width="70">&nbsp;</td>
									<td>&nbsp;</td>
									<td colspan="2"><div align="right">DCTO. TOTAL</div></td>
									<td colspan="2">
										<div align="right">
											<input name="txtDctoG" type="text" id="txtDctoG" style="width:100px" onkeyup="valoresG('txtDctoG')" value="<?php echo $dctoGen; ?>"/>
										</div>
									</td>
									<td>&nbsp;</td>
								</tr>
								<tr bgcolor="#FFFFFF">
									<td width="105">&nbsp;</td>
									<td width="110">&nbsp;</td>
									<td width="430">&nbsp;</td>
									<td width="70">&nbsp;</td>
									<td width="70"><input type="hidden" name="TotalReal" id="TotalReal" value="0" /></td>
									<td colspan="2"><div align="right">TOTAL&nbsp;</div></td>
									<td colspan="2"><div align="right"><input name="txtTotG" type="text" id="txtTotG" style="width:100px" /></div></td>
									<td>&nbsp;</td>
								</tr>
								<tr bgcolor="#FFFFFF">
									<td colspan="10">&nbsp;</td>
								</tr>
							</table>
							<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td align="center"><a href="carritoformal.php">Cotizar Formal</a></td>
									<td width="260" align="center"><input type="submit" name="ira4" value="Cotizar" onclick="cotizar()" /></td>
									<form method="post" action="borrar.php">
										<td width="260" align="center"><input type="submit" name="ira2" value="Limpiar Carrito" onclick="limpiar()" /></td>
									</form>
									<td width="260" align="center"><input type="submit" name="ira3" value="Comprar" onclick="comprar()" /></td>
								 </tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="contMain" name="contMain" style="height:100px;width:100%;background-color:#FFF"></div>
	<!-- jQuery -->
		<script src="js/jquery-1.11.0.min.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
			function cotizaformal() {
				window.location("carritoformal.php");
				return;
			}

			function valores(row,a,b,c,d,f) {
			 // row corresponde a la fila
			 // a = valor
			 // b = cantidad
			 // c = Descuento
			 // d = Total
			 // f = código producto
				valF = document.getElementById(f).value;
				var tabla = document.getElementById("carrito");
				tabla.innerHTML;
				if(valF) {
					var lineas = (carrito.rows.length - 6);
					var e = row.parentNode.parentNode.rowIndex;
					valA = a;
					if(isNaN(a)) {
						valA = document.getElementById(a).value;
					}
					valB = document.getElementById(b).value;
					valC = document.getElementById(c).value;
					if(valB == '') { return; }
				 // calculo entre valor y cantidad
					valD = valA * valB;
					if(valC.search("%") != -1) {
						res = valC.substr(0, valC.search("%"));
						valE = valD - ((valD*res)/100);
					} else {
						valE = valD - valC;
					}
				 // calculo incluye descuento
					valD = valE;
				 // asigna valor en la casilla total de la linea
					document.getElementById(d).value = valD;
					cntTxt = lineas;
					resT = 0;
					for(i=0;i<cntTxt;i++) {
						resT = resT + eval(document.getElementById("txtTot["+i+"]").value);
					}
					document.getElementById("txtTotG").value = resT;
					document.getElementById("TotalReal").value = resT;
				 // entregamos valores calculados a la Matrix
					$("#contMain").load("maincart.php?cod="+e+"&ac=sum&b="+valB+"&c="+valC+"&d="+valD);
				}
				valoresG('txtDctoG');
				return false;
			}

			function borraLinea(row) {
				var e = row.parentNode.parentNode.rowIndex;
				$("#contMain").load("maincart.php?cod="+e+"&ac=del");
				location.reload();
				return false;
			}

			function buscaprod(row) {
				var e = row.parentNode.parentNode.rowIndex;
				var valorbusqueda = document.getElementById('txtCart1').value;
				$("#contMain").load("maincart.php?cod="+e+"&ac=bus&valorbusqueda="+valorbusqueda);
			}

			function valoresG(dctoG) {
				valB = document.getElementById('TotalReal').value;
				valC = document.getElementById(dctoG).value;
				if(valC.search("%") != -1) {
					res = valC.substr(0, valC.search("%"));
					valE = valB - ((valB*res)/100);
				} else {
					valE = valB - valC;
				}
				valD = valE;
				document.getElementById('txtTotG').value = valD;
				$("#contMain").load("maincart.php?ac=dctoG&valdcto="+valC);
			}

			function cotizar() {
				clirut = document.getElementById("idRut").value;
				idvend = document.getElementById("idVend").value;
				if(idvend == '') {
					alert("Debe ingresar el código de vendedor");
					return;
				}
				if(clirut == '') {
					alert("Debe ingresar el RUT del cliente");
					return;
				}
				guardarcliente2();
				window.open("cotiza.php", "cotiza", "width=400,height=500,menubar=no,toolbar=no,location=no");
			}

			function limpiar() {
				$("#contMain").load("borrar.php");
			}

			function comprar() {
				clirut = document.getElementById("idRut").value;
				idvend = document.getElementById("idVend").value;
				if(idvend == '') {
					alert("Debe ingresar el código de vendedor");
					return;
				}
				if(clirut == '') {
					alert("Debe ingresar el RUT del cliente");
					return;
				}
				guardarcliente2();
				window.open("comprar.php", "cotiza", "width=400,height=500,menubar=no,toolbar=no,location=no");
			}

			function guardarcliente() {
				clirut = document.getElementById("idRut").value;
				clinom = document.getElementById("cliNom").value;
				clidir = document.getElementById("cliDir").value;
				clicomu = document.getElementById("cliComuna").value;
				$.post("maincart.php",
				{
					acc: "guardarcliente",
					dir: clidir,
					nom: clinom,
					comu: clicomu,
					rut: clirut
				},function(data,status) {
					alert("Cliente ingresado correctamente");
				});
			}
			
			function guardarcliente2() {
				clirut = document.getElementById("idRut").value;
				clinom = document.getElementById("cliNom").value;
				clidir = document.getElementById("cliDir").value;
				clicomu = document.getElementById("cliComuna").value;
				$.post("maincart.php",
				{
					acc: "guardarcliente",
					dir: clidir,
					nom: clinom,
					comu: clicomu,
					rut: clirut
				},function(data,status) {
					return;
				});
			}

			function checkTabPress(elemento) {
				if (event.keyCode == 9) {
					nombre = elemento.name;
					valor = elemento.value;
					if(nombre == 'idVend') {
						$("#contMain").load("maincart.php?idvend="+valor+"&ac=vend");
					}
					if(nombre == 'idRut') {
						if(valor != '') {
							$("#contMain").load("maincart.php?idrut="+valor+"&ac=rut");
						} else {
							alert("Ingrese un RUT");
							elemento.focus();
						}
					}
					if(nombre=='txtCart1') {
						var e = elemento.parentNode.parentNode.rowIndex;
						$("#contMain").load("maincart.php?cod="+e+"&ac=bus&valorbusqueda="+valor);
					}
				}
			}
			<?php
				echo "valI=".count($Matrix).";\n";
			?>
			valF = 0;
			for(i=0;i<valI;i++) {
				valF = valF + eval(document.getElementById("txtTot["+i+"]").value);
			}
			document.getElementById("txtTotG").value = valF;
			document.getElementById("TotalReal").value = valF;
			<?php
				if($dctoGen != '') {
					echo "valoresG('txtDctoG');";
				}
			?>
			if(document.getElementById('idRut').value) {
				$("#contMain").load("maincart.php?idrut="+document.getElementById('idRut').value+"&ac=rut");
			}
		</script>
	</body>
</html>