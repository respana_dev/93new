<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
include_once("page_template.html");
if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}
if(isset($_SESSION['idvend'])) {
	$idVendedor = $_SESSION['idvend'];
}
if(isset($_SESSION['idrut'])) {
	$idRut = $_SESSION['idrut'];
}
if(isset($_SESSION['dctoGen'])) {
	$dctoGen = $_SESSION['dctoGen'];
}

?>
<input type="hidden" name="cm" id="cm" value="<?php echo count($Matrix); ?>" />
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
        <div>
        <table width="1045" border="0" cellspacing="0" cellpadding="0" align="center" >
          <tr>
            <td width="120">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td width="120">Código Vendendor</td>
            <td width="10">:</td>
            <td><input type="text" name="idVend" value="<?php echo($idVendedor); ?>" id="idVend" onkeydown="checkTabPress(this)"/></td>
          </tr>
          <tr>
            <td>RUT Cliente</td>
            <td>:</td>
            <td><input type="text" name="idRut" value="<?php echo($idRut); ?>" id="idRut" onkeydown="checkTabPress(this)"/></td>
          </tr>
          <tr>
            <td width="120">&nbsp;</td>
            <td width="10">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table>
<table width="1045" border="0" align="center" cellpadding="0" cellspacing="0" id="carrito">
    <tr bgcolor="#CCCCCC">
            <td width="105">CodProd</td>
            <td width="90">CodSubGru</td>
            <td width="430">DesProd</td>
            <td width="70">Stock</td>
            <td width="70">PrecioBol</td>
            <td width="70">Cantidad</td>
            <td width="70">Dcto.</td>
            <td width="75">Total</td>
            <td width="70">Eliminar</td>
          </tr>
<?php
$i=0;
foreach($Matrix as $key=>$row) {
	
	echo '
	<input type="hidden" name="txtPrecio['.$i.']" id="txtPrecio['.$i.']" value="'.$row['PrecioBol'].'">
	<tr bgcolor="#FFFFFF">
            <td width="105"><label id="txt_1['.$i.']">'.$row[0].'</label></td>
            <td width="90"><label id="txt_2['.$i.']">'.$row[1].'</label></td>
            <td width="390"><label id="txt_3['.$i.']">'.$row[2].'</label></td>
            <td width="70"><label id="txt_4['.$i.']">'.number_format($row[3]).'</label></td>
            <td width="70"><label id="txt_5['.$i.']">$ '.number_format($row[4]).'</label></td>
            <td width="70"><input id="txtCant['.$i.']" name="txtCant['.$i.']" type="text" style="width:50px" onkeyup="valores(this,'.$row[4].',\'txtCant['.$i.']\',\'txtDesc['.$i.']\',\'txtTot['.$i.']\')" value="'.$row[5].'"></td>
			<td width="70"><input id="txtDesc['.$i.']" name="txtDesc['.$i.']" type="text" style="width:50px" onkeyup="valores(this,'.$row[4].',\'txtCant['.$i.']\',\'txtDesc['.$i.']\',\'txtTot['.$i.']\')" value="'.$row[6].'"></td>
			<td width="70"><input id="txtTot['.$i.']" name="txtTot['.$i.']" type="text" style="width:70px" value="'.$row[7].'"></td>
            <td width="70"><input type="button" style="width:50px" value="X" onclick="borraLinea(this)"></td>
    </tr>
	';
	$i++;
}
?>      
          <tr bgcolor="#FFFFFF">
            <td width="105"><input type="text" id="txtCart1" name="txtCart1" style="width:100px" onkeydown="checkTabPress(this)"/></td>
            <td width="90"><input type="text" id="txtCart3" name="txtCart3" style="width:80px"></td>
            <td width="430"><input type="text" id="txtCart4" name="txtCart4" style="width:370px"></td>
            <td width="70"><input type="text" id="txtCart5" name="txtCart5" style="width:50px"></td>
            <td><input type="text" id="txtCart6" name="txtCart6" style="width:50px"></td>
            <td><input type="text" id="txtCart7" name="txtCart7" style="width:50px" onKeyUp="valores(this,'txtCart6','txtCart7','txtCart8','txtCart9')"></td>
            <td><input type="text" id="txtCart8" name="txtCart8" style="width:50px" onKeyUp="valores(this,'txtCart6','txtCart7','txtCart8','txtCart9')"></td>
            <td width="75"><input type="text" id="txtCart9" name="txtCart9" style="width:70px"></td>
            <td><input type="button" style="width:50px" value=">" onclick="location.reload();"></td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td width="105">&nbsp;</td>
            <td width="110">&nbsp;</td>
            <td width="430">&nbsp;</td>
            <td width="70">&nbsp;</td>
            <td width="70">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="75">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td width="430">&nbsp;</td>
            <td width="70">&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="2"><div align="right">DCTO. TOTAL</div></td>
            <td colspan="2"><div align="right">
              <input name="txtDctoG" type="text" id="txtDctoG" style="width:100px" onkeyup="valoresG('txtDctoG')" value="<?php echo $dctoGen; ?>"/>
            </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
            <td width="105">&nbsp;</td>
            <td width="110">&nbsp;</td>
            <td width="430">&nbsp;</td>
            <td width="70">&nbsp;</td>
            <td width="70"><input type="hidden" name="TotalReal" id="TotalReal" value="0" /></td>
            <td colspan="2"><div align="right">TOTAL&nbsp;</div></td>
            <td colspan="2"><div align="right">
              <input name="txtTotG" type="text" id="txtTotG" style="width:100px" />
            </div></td>
            <td>&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
          <td colspan="10">&nbsp;</td>
          </tr>
          <tr bgcolor="#FFFFFF">
          <td colspan="3" align="center"><input type="submit" name="ira" value="Cotizar" onclick="cotizar()"></td>
          <td colspan="4" align="center"><input type="submit" name="ira" value="Limpiar Carrito" onclick="limpiar()"></td>
            <td colspan="3" align="center"><input type="submit" name="ira" value="Comprar" onclick="comprar()"></td>
            </tr>
            <tr bgcolor="#FFFFFF">
          <td colspan="10">&nbsp;</td>
          </tr>
          </table>
          
        </div>
</div>
<!-- Carrito compra -->
<div id="contMain" name="contMain" style="height:100px;width:200px;background-color:#FFF"></div>
<!-- jQuery -->
<script src="js/jquery-1.11.0.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<script language="javascript">
function valores(row,a,b,c,d) {
	var e = row.parentNode.parentNode.rowIndex;
	valA=a;
	if(isNaN(a)) {
		valA=document.getElementById(a).value;
	}
	valB=document.getElementById(b).value;
	valC=document.getElementById(c).value;
	valD=valA*valB;
	if(valC.search("%")!=-1) {
		res=valC.substr(0,valC.search("%"));
		valE=valD-((valD*res)/100);
	} else {
		valE=valD-valC;
	}
	valD=valE;
	document.getElementById(d).value = valD;
	<?php
	echo "cntTxt=".count($Matrix).";\n";
	?>
	resT=0;
	for(i=0;i<cntTxt;i++) {
		resT=resT+eval(document.getElementById("txtTot["+i+"]").value);
	}
	document.getElementById("txtTotG").value = resT;
	document.getElementById("TotalReal").value = resT;
	$("#contMain").load("maincart.php?cod="+e+"&ac=sum&b="+valB+"&c="+valC+"&d="+valD);
	return false;
	//valD=document.getElementById(d).value;
}
function borraLinea(row) {
	var e = row.parentNode.parentNode.rowIndex;
	//$("#contMain").load("maincart.php?cod="+e+"&ac=del");
    //document.getElementsByTagName("table")[0].setAttribute("id", "carrito);
	//document.getElementById("hola").innerHTML = "poto";
	
	var cntTxt = document.getElementById("cm").value;
	var valor = document.getElementById("TotalReal").value;
	var resT=0;
	var i = 0;
	var f = e - 1;
	var resT = document.getElementById("txtTot["+(f)+"]").value;
	resT = valor-resT;
	for(i = f; i < cntTxt-1; i++) {
		j = i + 1;
		document.getElementById("txt_1["+i+"]").innerHTML = document.getElementById("txt_1["+j+"]").innerHTML;
		document.getElementById("txt_2["+i+"]").innerHTML = document.getElementById("txt_2["+j+"]").innerHTML;
		document.getElementById("txt_3["+i+"]").innerHTML = document.getElementById("txt_3["+j+"]").innerHTML;
		document.getElementById("txt_4["+i+"]").innerHTML = document.getElementById("txt_4["+j+"]").innerHTML;
		document.getElementById("txt_5["+i+"]").innerHTML = document.getElementById("txt_5["+j+"]").innerHTML;
		document.getElementById("txtPrecio["+i+"]").value = document.getElementById("txtPrecio["+j+"]").value;
		document.getElementById("txtCant["+i+"]").value = document.getElementById("txtCant["+j+"]").value;
		document.getElementById("txtDesc["+i+"]").value = document.getElementById("txtDesc["+j+"]").value;
		document.getElementById("txtTot["+i+"]").value = document.getElementById("txtTot["+j+"]").value;
	}
	document.getElementById("carrito").deleteRow(cntTxt);
	
	document.getElementById("cm").value = cntTxt - 1;
	document.getElementById("txtTotG").value = resT;
	document.getElementById("TotalReal").value = resT;
	
	


	$("#contMain").load("maincart.php?cod="+e+"&ac=del");
	return false;
}
function buscaprod(row) {
	var e = row.parentNode.parentNode.rowIndex;
	var valorbusqueda = document.getElementById('txtCart1').value;
	$("#contMain").load("maincart.php?cod="+e+"&ac=bus&valorbusqueda="+valorbusqueda);
}
function valoresG(dctoG) {
	//var e = row.parentNode.parentNode.rowIndex;
	valB=document.getElementById('TotalReal').value;
	valC=document.getElementById(dctoG).value;
	if(valC.search("%")!=-1) {
		res=valC.substr(0,valC.search("%"));
		valE=valB-((valB*res)/100);
	} else {
		valE=valB-valC;
	}
	valD=valE;
	document.getElementById('txtTotG').value = valD;
	//$("#contMain").load("maincart.php?idvend="+valor+"&ac=vend");
	$("#contMain").load("maincart.php?ac=dctoG&valdcto="+valC);
}
function cotizar() {
	window.open("cotiza.php","cotiza","width=400,height=500,menubar=no,toolbar=no,location=no");
}
function limpiar() {
	$("#contMain").load("maincart.php?ac=limpiar");
	location.reload();
}
function comprar() {
	window.open("comprar.php","cotiza","width=400,height=500,menubar=no,toolbar=no,location=no");
}
function checkTabPress(elemento) {
    if (event.keyCode == 9) {
		nombre=elemento.name;
		valor=elemento.value;
		if(nombre=='idVend') {
			$("#contMain").load("maincart.php?idvend="+valor+"&ac=vend");
		}
		if(nombre=='idRut') {
			$("#contMain").load("maincart.php?idrut="+valor+"&ac=rut");
		}
		if(nombre=='txtCart1') {
			var e = elemento.parentNode.parentNode.rowIndex;
			$("#contMain").load("maincart.php?cod="+e+"&ac=bus&valorbusqueda="+valor);
		}
		if(nombre=='') {
			
		}
    }
}

<?php
echo "valI=".count($Matrix).";\n";
?>
valF=0;
for(i=0;i<valI;i++) {
	valF=valF+eval(document.getElementById("txtTot["+i+"]").value);
}
document.getElementById("txtTotG").value = valF;
document.getElementById("TotalReal").value = valF;
<?php
if($dctoGen!='') {
	echo "valoresG('txtDctoG');";
}
?>
</script>
</body>
</html>
