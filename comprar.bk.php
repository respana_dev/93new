<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$dbcon = odbc_connect('SREL', 'sa', 'SRel9300');
$dbcon2 = odbc_connect('COTI', 'sa', 'SRel9300');

if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}

$idvend = $_SESSION['idvend'];
$idcliente = $_SESSION['idrut'];
$idcliente2 = substr($idcliente,0,strpos($idcliente,"-"));

$dctoCot = $_SESSION['dctoGen'];

$sql="SELECT max(idCot)+1 AS CodCot FROM Cot_general";
$rs = odbc_exec($dbcon2,$sql);
$row = odbc_fetch_array($rs);
$CodCot = $row['CodCot'];

$sql="SELECT max(NVNumero)+1 AS nv FROM softland.nw_nventa";
$rs = odbc_exec($dbcon,$sql);
$row = odbc_fetch_array($rs);
$NVNumero = $row['nv'];

$sql="SELECT * FROM softland.cwtauxi WHERE (CodAux = '$idcliente2')";
$rs = odbc_exec($dbcon,$sql);
$row = odbc_fetch_array($rs);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>

<style type="text/css">
.table {
	font-size: 12px;
	font-weight: bold;
	font-family: Tahoma, Geneva, sans-serif;
}
.tahoma11 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
.tahoma12 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
}
.tahoma12negrita {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	font-weight:bold;
}
.tahoma111 {	font-family: Verdana, Geneva, sans-serif;
	font-size: 11.5px;
}
</style>
</head>

<body>
<table width="300" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><img src="img/logo.jpg" alt="" width="220" height="86" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><span class="tahoma12negrita">Nota de Venta <?php echo $NVNumero; ?></span></td>
  </tr>
  <tr>
    <td align="center" class="tahoma12negrita">Fecha Emisión: <?php echo date("d-m-Y"); ?></td>
  </tr>
  <tr>
    <td align="center" class="tahoma12negrita">Vendedor: <?php echo $idvend; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="300" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="300" border="0" cellpadding="0" cellspacing="0">
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290">&nbsp;</td>
      </tr>
<?php
$valorTotal=0;
$valTotalSIVA=0;
$i = 1;
foreach($Matrix as $key=>$row) {
	$valProdSIVA = ceil($row[4]/1.19);
	$valLineaSIVA = $valProdSIVA * $row[5];
	
	$valorTotal=$row[7] + $valorTotal;
	$valorLinea = $row[4] * $row[5];
	
	if($row[6]!=0) {
		$valxl=strpos($row[6],'%');
		if($valxl!=0) {
			$res=substr($row[6],0,$valxl);
			$dctoPtjeLin = $res;
			$dctoValorLin = (($valorLinea*$res)/100);
		} else {
			$dctoPtjeLin = round((($row[6]*100)/$valorLinea),1);
			$dctoValorLin = $row[6];
		}	
		$valDctoSIVA = ceil($dctoValorLin/1.19);
	}
	$valLineaSIVAS = $valLineaSIVA - $valDctoSIVA;
	$sqldet = $sqldet. "INSERT INTO softland.nw_detnv (NVNumero, nvLinea, nvCorrela, nvFecCompr, CodProd, nvCant, nvPrecio, nvEquiv, nvSubTotal, nvDPorcDesc01, nvDDescto01, nvTotDesc, nvTotLinea, DetProd, CheckeoMovporAlarmaVtas, CantUVta) VALUES 
('$NVNumero', '$i', '0', CONVERT (date, GETDATE()), '$row[0]', '$row[5]', '$valProdSIVA', '1', '$valLineaSIVA', '$dctoPtjeLin', '$valDctoSIVA', '$valDctoSIVA', '$valLineaSIVAS', '$row[2]', 'N', '$row[5]')  ";
	//$rs = odbc_exec($dbcon,$sql);	
	$i++;
	echo '
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290">'.substr($row[2],0,39).'</td>
      </tr>
      <tr class="tahoma111">
        <td>&nbsp;</td>
        <td>
		<table width="290" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><strong>'.$row[0].'</strong></td>
            <td width="20"><strong>'.$row[5].'</strong></td>
			<td width="10"><strong>x</strong></td>
            <td align="right"><strong>$ '.number_format($row[4],0,',','.').'&nbsp;</strong></td>
            <td width="10" align="center"><strong>=</strong></td>
            <td width="80" align="right"><strong>$ '.number_format($row[7],0,',','.').'&nbsp;</strong></td>
          </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
';
  $valTotalSIVA = $valTotalSIVA + $valLineaSIVAS;
}

if($dctoCot!=0) {
	$valx=strpos($dctoCot,'%');
}
if($valx!=0) {
	$res=substr($dctoCot,0,$valx);
	$res2=$valorTotal-(($valorTotal*$res)/100);
	$valE=$res2;
	$dctoPtje = $res;
	$dctoValor = (($valorTotal*$res)/100);
} else {
	$valE=$valorTotal-$dctoCot;
	$dctoPtje = (($dctoCot*100)/$valorTotal);
	$dctoValor = $dctoCot;
}
$valDctoTotalSIVA = ceil($dctoValor/1.19);
$valorSIVA = ($valorTotal / 1.19);
$valorIVA = ($valTotalSIVA * 1.19);

$sql = "INSERT INTO softland.nw_nventa (NVNumero, nvFem, nvEstado, CotNum, NumOC, nvFeEnt, CodAux, VenCod, CodMon, nvObser, NomCon, nvSubTotal, nvPorcDesc01, nvDescto01, nvMonto, nvEquiv, nvNetoAfecto, UsuarioGeneraDocto, FechaHoraCreacion, Sistema, ConcManual, proceso, TotalBoleta) VALUES 
('$NVNumero', CONVERT (date, GETDATE()), 'A', '0', '0', CONVERT (date, GETDATE()), '$idcliente2', '$idvend', '01', '$idcliente', '', '$valTotalSIVA', '$dctoPtje', '$valDctoTotalSIVA', '$valorIVA', '1', '$valTotalSIVA', 'mespana', CONVERT (date, GETDATE()), 'NW', 'N', 'Notas de Venta', '$valorIVA')";
//echo $sql;
//echo "<br>";
$rs = odbc_exec($dbcon,$sql);
$rs = odbc_exec($dbcon,$sqldet);
//echo $sqldet;
//echo "<br>";
?>
    </table></td>
  </tr>
</table>
<table width="300" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="300" border="0" cellpadding="0" cellspacing="0">
      <tr class="tahoma111">
        <td width="10"></td>
        <td width="290"><table width="290" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><strong>Total&nbsp;</strong></td>
            <td width="10"><strong>=</strong></td>
            <td width="100" align="right"><strong><?php echo '&nbsp;$ '.number_format($valorTotal,0,',','.'); ?></strong>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br />
<!--
<0A0A0A0A0A0A0A0A0A0A0A0A>
-->
</body>
</html>
<script>
window.print(false);
</script>