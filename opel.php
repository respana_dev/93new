<?php
require_once("cache.php");
require_once("conf.php"); 
include_once("page_template.html");
?>
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
 <?php

$dg = new C_DataGrid ("SELECT CodProd, CodigoBarra, DesProd2, CodSubGru, DesProd, Stock, PrecioBol, Ubicacion, Inactivo FROM ViewStockSrelProd", "CodProd","OPEL");

$dg-> set_query_filter("CodGrupo = '12'AND Inactivo='0'");
//$dg-> set_query_filter("CodSubGru= 'MOT'");
//$dg-> set_col_hidden("Inactivo");

//$dg->enable_edit("INLINE","CRUD"); 

$dg -> set_theme('aristo');
//$dg-> set_col_title("DesProd", "DESCRIPCION");
$dg-> set_col_width("CodProd", 105);
$dg-> set_col_width("CodigoBarra", 100);
$dg-> set_col_width("DesProd2", 110);
$dg-> set_col_width("CodSubGru", 90);
$dg-> set_col_width("DesProd", 390);
$dg-> set_col_width("Stock", 70);
$dg-> set_col_width("PrecioBol", 70);
//$dg-> set_col_edittype ("DesProd","textarea");
//$dg->enable_edit("FORM","CRUD");
	
//$dg -> set_col_default('Impuesto', '-1');
//$dg->set_col_default('CodUMed','01');
//$dg -> set_col_hidden("Impuesto");
//$dg -> set_col_hidden("CodUMed");
// $dg -> set_col_readonly("Impuesto"); 
$dg -> set_col_edittype("CodSubGru", "select", "AMO:Amortiguadores;CAR:Carroceria;COR:Correas;FIL:Filtros;EMB:Embrague;ENC:Encendido;FRE:Frenos;DIS:Distribucion;MOT:Motor;EMP:Empaquetaduras;REF:Refrigeracion;SUS:Suspension;TRA:Transmision;UNI:Universal;LUB:Lubricantes", false); 
//$dg-> set_col_currency("PrecioVta","$","",",",2,"0");
//$dg-> set_col_currency("PrecioBol","$","",",",2,"0");
//$dg->enable_kb_nav(true);
$dg->set_conditional_format("Stock","CELL",array("condition"=>"gt",
                                                      "value"=>"0",
                                                      "css"=> array("color"=>"#ffffff","background-color"=>"green")));

$dg->set_conditional_format("Stock","CELL",array("condition"=>"le",
                                                  "value"=>"0",
                                                  "css"=> array("color"=>"red","background-color"=>"#DCDCDC")));

$dg->set_locale('es');
$dg -> enable_search(true);
$dg -> enable_export('excel');
//$dg->enable_advanced_search(true);
$dg -> set_col_format('PrecioVta','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
$dg -> set_col_format('PrecioBol','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
$dg -> set_col_format('Stock','integer', array('thousandsSeparator'=>'.', 'defaultValue'=>'0'));
$dg-> set_col_dynalink("CodProd","http://iis/91/app.php","CodProd");
$dg->enable_debug(false);
$dg -> set_dimension(1020, 450);
$dg -> set_multiselect(true, true);
$dg -> display();?>
<div id="contMain" name="contMain" style="height:1px;width:1px"></div>
    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
</body>
</html>
<script language="javascript">
$('#page-wrapper').click(function(){
	var rows = getSelRows();
	$("#contMain").load("main.php?cod="+rows);
});

</script>