		<?php
			include_once("dbcon.php");
			require_once("../../93new/cache.php");
			require_once("../../93new/conf.php"); 
			include_once("../../93new/page_template.html");

			function rellena_marca() {
				global $dbcon;
				$sql = "SELECT * FROM Srel.softland.iw_tgrupo WHERE PubGrupo = 'S'";
				$rs = odbc_exec($dbcon, $sql);
				while($row = odbc_fetch_array($rs)) {
					echo '<option value="'.$row['CodGrupo'].'">'.$row['DesGrupo'].'</option>';
				}
			}
		?>
		<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" >
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
								</tr>
								<tr align="center">
									<td width="400">&nbsp;</td>
									<td width="60">Marca : </td>
									<td colspan="1">
										<select name="marca" id="marca">
											<option value="0">Seleccionar</option>
											<?php rellena_marca();?>
										</select>
										(Seleccione la marca)
									</td>
								</tr>
								<tr>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
									<td width="120">&nbsp;</td>
									<td width="10">&nbsp;</td>
									<td width="392">&nbsp;</td>
								</tr>
							</table>
							<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
								<tr align="center">
									<td width="33%">
										<p align="center"><font size="3" color="black">Vehículo</font></p>
										<select name="vehiculo" id="vehiculo" size="20" style="width: 310px;"></select>
									</td>
									<td>&nbsp;</td>
									<td width="33%">
										<p align="center"><font size="3" color="black">Aplicación</font></p>
										<select name="aplicacion" id="aplicacion" size="20" style="width: 310px;"></select>
									</td>
									<td>&nbsp;</td>
									<td width="33%">
										<p align="center"><font size="3" color="black">Repuesto </font><input type="text" name="txtbusca" id="txtbusca"></p>
										<select name="repuesto" size="20" multiple="MULTIPLE" id="repuesto" style="width: 310px;"></select>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script>
			$("#marca").change(function() {
				var val = "";
				val = $( this ).val();
				$("#vehiculo").load('list.php?op=1&marca='+val);
				$("#aplicacion").load('list.php?op=8');
				$("#repuesto").load('list.php?op=8');
			})

			$("#vehiculo").change(function(){
				var val = "";
				val += $(this).val();
				$("#aplicacion").load('list.php?op=2');
				$("#repuesto").load('list.php?op=8');
			})

			$("#aplicacion").change(function(){
				var val = "";
				$("select option:selected").each(function() {
					val += $(this).val() + ";";
				});
				$("#repuesto").load('list.php?op=3&valap='+val);
			})

			$("#repuesto").click(function(){
				var val = "";
				var buscar = "";
				buscar = $("#txtbusca").val();
				$("select option:selected").each(function() {
					val += $(this).val() + ";";
				});
				if(buscar) {
					$("#repuesto").load('list.php?op=10&valap='+val+'&txt='+buscar);
				} else {
					$("#repuesto").load('list.php?op=4&valap='+val);
				}
			})
			
			$("#txtbusca").keyup(function() {
				var buscar = "";
				var val = "";
				buscar = $(this).val();
				$("select option:selected").each(function() {
					val += $(this).val() + ";";
				});
				$("#repuesto").load('list.php?op=9&valap='+val+'&txt='+buscar);
			})
		</script>
	</body>
</html>