<?php
header("Cache-Control: no-store, no-cache, must-revalidate");
include_once("dbcon.php");

$marca=$_REQUEST['marca'];
$op = $_REQUEST['op'];
$valap = $_REQUEST['valap'];
$txt = $_REQUEST['txt'];

switch($op) {
	case 1:
		llena_vehiculo($marca,$dbcon);
		break;
	case 2:
		llena_aplicacion($dbcon);
		break;
	case 3:
		llena_repuesto($valap,$dbcon);
		break;
	case 4:
		graba_repuesto($valap,$dbcon);
		break;
	case 5:
		llena_repuesto2($valap,$dbcon);
		break;
	case 6:
		llena_vehiculo2($valap,$dbcon);
		break;
	case 7:
		graba_repuesto2($valap,$dbcon);
		break;
	case 8:
		borra_lista();
		break;
	case 9:
		llena_repuesto3($valap,$txt,$dbcon);
		break;
	case 10:
		graba_repuesto3($valap,$txt,$dbcon);
		break;
}

function llena_vehiculo($marca,$dbcon) {
	$sql = "SELECT [codVehiculo], [descrip], [motor], [ano] FROM [srel].[dbo].[vehiculos] WHERE [CodGrupo] = '$marca' ORDER BY [descrip]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['codVehiculo'].'">'.$row['descrip'].' - '.$row['motor'].'-'.$row['ano'].'</option>';
	}
}

function llena_vehiculo2($tipoapp,$dbcon) {
	$valores = explode(';',$tipoapp);
	$sql = "SELECT [codVehiculo], [descrip], [ano] FROM [srel].[dbo].[vehiculos] ORDER BY [descrip]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['codVehiculo'].'" '.marca_repuesto($row['codVehiculo'],$valores[1],$dbcon).'>'.$row['descrip'].' - '.$row['ano'].'</option>';
	}
}

function llena_aplicacion($dbcon) {
	$sql = "SELECT [CodSubGr], [DesSubGr] FROM [SREL].[softland].[iw_tsubgr]  ORDER BY [DesSubGr]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['CodSubGr'].'">'.$row['DesSubGr'].'</option>';
	}
}

function llena_repuesto($tipoapp,$dbcon) {
	$valores = explode(';',$tipoapp);
	$sql = "SELECT [CodProd], [DesProd] FROM [SREL].[softland].[iw_tprod] WHERE [CodSubGr] = '$valores[2]' ORDER BY [DesProd]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['CodProd'].'" '.marca_repuesto($valores[1],$row['CodProd'],$dbcon).'>'.$row['CodProd'].' - '.$row['DesProd'].'</option>';
	}
}

function llena_repuesto2($tipoapp,$dbcon) {
	$sql = "SELECT [CodProd], [DesProd] FROM [SREL].[softland].[iw_tprod] WHERE [CodSubGr] = '$tipoapp' ORDER BY [DesProd]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['CodProd'].'">'.$row['CodProd'].' - '.$row['DesProd'].'</option>';
	}
}

function llena_repuesto3($tipoapp,$txt,$dbcon) {
	$valores = explode(';',$tipoapp);
	$sql = "SELECT [CodProd], [DesProd] FROM [SREL].[softland].[iw_tprod] WHERE [CodSubGr] = '$valores[2]' AND ([DesProd] like '%$txt%' OR [CodProd] like '%$txt%') ORDER BY [DesProd]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['CodProd'].'" '.marca_repuesto($valores[1],$row['CodProd'],$dbcon).'>'.$row['CodProd'].' - '.$row['DesProd'].'</option>';
	}
}

function marca_repuesto($vehiculo,$repuesto,$dbcon){
	$sql = "SELECT * FROM [srel].[dbo].[prod_vehiculos] WHERE [codVehiculo] = '$vehiculo' AND [CodProd] = '$repuesto'";
	$rs = odbc_exec($dbcon, $sql);
	$x = odbc_num_rows($rs);
	if($x>0) {
		return "selected";
	} 	
}

function graba_repuesto($tipoapp,$dbcon) {
	$valores = explode(';',$tipoapp);
	if(marca_repuesto($valores[1],$valores[3],$dbcon)!="selected") {
		$sql = "INSERT INTO [SREL].[dbo].[prod_vehiculos] VALUES ('$valores[3]','$valores[1]')";	
	} else {
		$sql = "DELETE FROM [SREL].[dbo].[prod_vehiculos] WHERE [CodProd] = '$valores[3]' AND [codVehiculo] = '$valores[1]'";	
	}
	$rs = odbc_exec($dbcon, $sql);
	llena_repuesto($tipoapp,$dbcon);
}

function graba_repuesto2($tipoapp,$dbcon) {
	$valores = explode(';',$tipoapp);
	$ira=marca_repuesto($valores[2],$valores[1],$dbcon);
	if($ira!="selected") {
		$sql = "INSERT INTO [SREL].[dbo].[prod_vehiculos] VALUES ('$valores[1]','$valores[2]')";
	} else {
		$sql = "DELETE FROM [SREL].[dbo].[prod_vehiculos] WHERE [CodProd] = '$valores[1]' AND [codVehiculo] = '$valores[2]'";
	}
	$rs = odbc_exec($dbcon, $sql);
	//borra_lista();
	llena_vehiculo2($tipoapp,$dbcon);
}

function graba_repuesto3($tipoapp,$txt,$dbcon) {
	$valores = explode(';',$tipoapp);
	if(marca_repuesto($valores[1],$valores[3],$dbcon)!="selected") {
		$sql = "INSERT INTO [SREL].[dbo].[prod_vehiculos] VALUES ('$valores[3]','$valores[1]')";	
	} else {
		$sql = "DELETE FROM [SREL].[dbo].[prod_vehiculos] WHERE [CodProd] = '$valores[3]' AND [codVehiculo] = '$valores[1]'";	
	}
	$rs = odbc_exec($dbcon, $sql);
	llena_repuesto3($tipoapp,$txt,$dbcon);
}

function borra_lista() {
	echo '<option value="0"></option>';
}
?>