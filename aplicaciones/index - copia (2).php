<?php
include_once("dbcon.php");

function rellena_marca() {
	global $dbcon;
	$sql = 'SELECT * FROM [srel].[softland].[iw_tgrupo]';
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['CodGrupo'].'">'.$row['DesGrupo'].'</option>';
	}
}
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Reponer Stock</title>
</head>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" type="text/css" href="/930/css/css/321381.css" />
<link rel="stylesheet" type="text/css" href="/930/css/style.css" />
<body>
<?php include_once("../header.php"); ?>
<p class="texto12bold">Aplicaciones</p>
<p class="texto12bold">Vehiculos / <a href="repuesto.php">Repuesto</a></p>
<br>
<div id="wrapper">
<div id="headerwrap">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr class="texto12bold">
	<td width="10">&nbsp;</td>
    <td width="200" id="header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><p>Marca</p></td>
        </tr>
        <tr>
          <td align="center">&nbsp;</td>
        </tr>
        <tr>
          <td align="center">
            <select name="marca" id="marca" size="20" style="width: 180px; overflow: auto;">
              <?php rellena_marca(); ?>
            </select>
          </td>
        </tr>
        <tr>
          <td align="center">&nbsp;</td>
        </tr>
      </table></td>
    <td width="10">&nbsp;</td>
    <td id="header" width="300px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><p>Veh&iacute;culo</p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center">
          <select name="vehiculo" id="vehiculo" size="20" style="width: 280px; overflow: auto;">
            </select>
          </td>
        </tr>
      </table></td>
    <td width="10">&nbsp;</td>
    <td width="300" align="center" id="header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><p>Aplicaci&oacute;n</p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><select name="aplicacion" id="aplicacion" size="20" style="width: 280px; overflow: auto;">
        </select></td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
    <td width="400" align="center" id="header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><p>Repuesto</p></td>
      </tr>
       <tr>
        <td>&nbsp;&nbsp;&nbsp;<input type="text" name="txtbusca" id="txtbusca"></td>
      </tr>
     <tr>
        <td align="center"><select name="repuesto" size="20" multiple="MULTIPLE" id="repuesto" style="width: 380px; overflow: auto;">
        </select></td>
      </tr>
    </table></td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script>
$( "#marca" )
  .change(function() {
    var val = "";
    val = $( this ).val();
	$("#vehiculo").load('list.php?op=1&marca='+val);
	$("#aplicacion").load('list.php?op=8');
	$("#repuesto").load('list.php?op=8');
})
$("#vehiculo") 
	.change(function(){
	var val = "";
    val += $(this).val();
	$("#aplicacion").load('list.php?op=2');
	$("#repuesto").load('list.php?op=8');
})
$("#aplicacion") 
	.change(function(){
	var val = "";
	$("select option:selected").each(function() {
        val += $(this).val() + ";";
    });
	$("#repuesto").load('list.php?op=3&valap='+val);
})
$("#repuesto")
	.click(function(){
	var val = "";
	$("select option:selected").each(function() {
        val += $(this).val() + ";";
    });
	$("#repuesto").load('list.php?op=4&valap='+val);
})
$("#txtbusca")
	.keyup(function() {
	var buscar = "";
	var val = "";
	buscar = $(this).val();
	$("select option:selected").each(function() {
        val += $(this).val() + ";";
    });
	$("#repuesto").load('list.php?op=9&valap='+val+'&txt='+buscar);
})
</script>
</body>
</html>