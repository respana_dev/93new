<?php
include_once("dbcon.php");

function rellena_aplicacion() {
	global $dbcon;
	$sql = "SELECT [CodSubGr], [DesSubGr] FROM [SREL].[softland].[iw_tsubgr] ORDER BY [DesSubGr]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['CodSubGr'].'">'.$row['DesSubGr'].'</option>';
	}
}

function rellena_vehiculo() {
	global $dbcon;
	$sql = " SELECT [codVehiculo], [descrip], [ano] FROM [srel].[dbo].[vehiculos] ORDER BY [descrip]";
	$rs = odbc_exec($dbcon, $sql);
	while($row = odbc_fetch_array($rs)) {
		echo '<option value="'.$row['codVehiculo'].'">'.$row['descrip'].' - '.$row['ano'].'</option>';
	}
}

?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Reponer Stock</title>
</head>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<link rel="stylesheet" type="text/css" href="/930/css/css/321381.css" />
<link rel="stylesheet" type="text/css" href="/930/css/style.css" />
<body>
<?php include_once("../header.php"); ?>
<p class="texto12bold">Aplicaciones</p>
<p class="texto12bold"><a href="index.php">Vehiculos</a> / Repuesto</p>
<br>
<div id="wrapper">
<div id="headerwrap">
<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr class="texto12bold">
  <td width="10">&nbsp;</td>
<td width="300" align="center" id="header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><p>Aplicaci&oacute;n</p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><select name="aplicacion" id="aplicacion" size="20" style="width: 280px; overflow: auto;">
        <?php rellena_aplicacion(); ?>
        </select></td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
    <td width="400" align="center" id="header"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td align="center"><p>Repuesto</p></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><select name="repuesto" size="20" id="repuesto" style="width: 380px; overflow: auto;">
        </select></td>
      </tr>
    </table></td>
	<td width="10">&nbsp;</td>        
    <td id="header" width="300px"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center"><p>Veh&iacute;culo</p></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center">
          <select name="vehiculo" id="vehiculo" size="20"  multiple="MULTIPLE" style="width: 280px; overflow: auto;">
          <?php rellena_vehiculo(); ?>
            </select>
          </td>
        </tr>
      </table></td>
    <td>&nbsp;</td>
  </tr>
</table>
</div>
</div>
<script>
$("#vehiculo") 
	.click(function(){
	var val = "";
	$("select option:selected").each(function() {
        val += $(this).val() + ";";
    });
	$("#vehiculo").load('list.php?op=7&valap='+val);
})
$("#aplicacion") 
	.change(function(){
	var val = "";
	val += $(this).val();
	$("#repuesto").load('list.php?op=5&valap='+val);
})
$("#repuesto") 
	.change(function(){
	var val = "";
	$("select option:selected").each(function() {
        val += $(this).val() + ";";
    });
	$("#vehiculo").load('list.php?op=6&valap='+val);
})
</script>
</body>
</html>