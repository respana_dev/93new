		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
		?>
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<?php
							/*  realizamos la conexion mediante odbc
								$cid=odbc_connect($dsn, $usuario, $clave); */
							$cid = odbc_connect('SREL', 'sa', 'SRel9300');
							
							if (!$cid){
								exit("<strong>Ha ocurrido un error tratando de conectarse con la base de datos.</strong>");
							}	

							if ($_GET['CodProd']<>0) { 
								$codigo = $_GET['CodProd'];
								echo '<div id="contentliquid">
										<div id="contentwrap">
											<div id="content">';	
	
								$sql = "SELECT CodProd, DesProd, DesProd2 
										FROM Red.softland.iw_tprod 
										WHERE CodProd = '$codigo' 
										ORDER BY DesProd";
								$rs = odbc_exec($cid, $sql);
								while($row = odbc_fetch_array($rs)) {
									echo '<font color="white">
											<option value="'.$row['CodProd'].'">Producto Seleccionado: '.$row['DesProd'].' - '.$row['DesProd2'].' - '.$row['CodProd'].' aplica a los siguientes vehiculos:</option>
										  </font>';
								}		

								$dg = new C_DataGrid ("SELECT prod_vehiculos.CodProd, vehiculos.marca, vehiculos.modelo, vehiculos.descrip, vehiculos.motor, vehiculos.chasis, vehiculos.ano, vehiculos.obs 
													  FROM prod_vehiculos INNER JOIN vehiculos ON prod_vehiculos.codVehiculo = vehiculos.codVehiculo", "prod_vehiculos.CodProd", "prod_vehiculos" );
								$dg-> set_query_filter("prod_vehiculos.CodProd = '$codigo'");

								$dg -> set_theme('aristo');
								$dg -> set_col_width("prod_vehiculos.CodProd", 105);
								$dg -> set_col_width("vehiculos.marca", 90);
								$dg -> set_col_width("vehiculos.modelo", 100);
								$dg -> set_col_width("vehiculos.descrip", 390);
								$dg -> set_col_width("vehiculos.motor", 100);
								$dg -> set_col_width("vehiculos.chasis", 100);
								$dg -> set_col_width("vehiculos.ano", 70);
								$dg -> set_col_width("vehiculos.obs", 150);
								
								$dg -> set_col_title("prod_vehiculos.CodProd", "C�digo");
								$dg -> set_col_title("vehiculos.marca", "Marca");
								$dg -> set_col_title("vehiculos.modelo", "Modelo");
								$dg -> set_col_title("vehiculos.descrip", "Descripci�n");
								$dg -> set_col_title("vehiculos.motor", "Motor");
								$dg -> set_col_title("vehiculos.chasis", "Chasis");
								$dg -> set_col_title("vehiculos.ano", "A�o");
								$dg -> set_col_title("vehiculos.obs", "Observaciones");
								 
								$dg -> enable_edit("FORM", "CRUD");
								$dg -> set_pagesize(30);
								
								$dg -> enable_search(true);
								$dg -> enable_export('excel');
								
								$dg -> enable_debug(false);
								$dg -> set_dimension(1080, 450);
								$dg -> enable_kb_nav(true);
								$dg -> set_locale('es');
								$dg -> display();

								echo '</div>
										</div>
											</div>'; 
							} 
						?>
						<td width="10">&nbsp;</td>
						<td width="0" align="" id="header">
							<table width="0" border="0" cellspacing="0" cellpadding="0">
								<tr></tr>
								<tr></tr>
							</table>
						</td>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
