<?php
require_once("conf.php");   
?>
   


<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SREL930</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">SREL930</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                    <a href="mantenciones.php" ><i class="fa fa-fw fa-wrench"></i>Fichas Mantenciones<b></b></a>
                    
                </li>
                <li class="dropdown">
                    <a href="catalogos.php" ><i class="fa fa-fw fa-wrench"></i>Catalogos<b></b></a>
                    
                </li>
			 <li class="dropdown">
                    <a href="#" ><i class=""></i>Aplicaciones<b></b></a>
                    
                </li>
			             <li class="dropdown">
                    
                  
                    
                </li>
			             <li class="dropdown">
                    <a href="modelo.php"></i> Lista x Marca<b></b></a>
                    
                </li>
                <li class="dropdown">
                    <a href="stock_reponer.php" ><i class=""></i> Reponer Stock<b></b></a>
                   
                </li>
               <li class="dropdown">
                    <a href="devoluciones.php" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-arrows-v"></i>Devoluciones<b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="dev_pendiente.php"><span class="label label-default">Pendiente</span></a>
                        </li>
                       
                        <li>
                            <a href="dev_ok.php"><span class="label label-success">OK</span></a>
                        </li>
                        
                        <li>
                            <a href="dev_despachado.php"><span class="label label-warning">Despachado</span></a>
                        </li>
                        <li>
                            <a href="dev_rechazado.php"><span class="label label-danger">Rechazado</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="devoluciones.php">TODOS</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-table"></i> Pedidos <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="pedidosmsa.php"><i class="fa fa-fw fa-user"></i> MSA</a>
                        </li>
                        <li>
                            <a href="pedidosals.php"><i class="fa fa-fw fa-user"></i> ALS</a>
                        </li>
                        <li>
                            <a href="pedidosref.php"><i class="fa fa-fw fa-user"></i> REFAX</a>
                        </li>
						<li>
                            <a href="pedidosnor.php"><i class="fa fa-fw fa-user"></i> NORIEGA</a>
                        </li>
                         <li>
                            <a href="pedidosgim.php"><i class="fa fa-fw fa-user"></i> GIMPORT</a>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                            <a href="pedidos.php"><i class="fa fa-fw fa-user"></i> TODOS</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li >
                        <a href="index.php"><i class=""></i> TODOS</a>
                    </li>
                    <li>
                        <a href="nissan.php"><i class=""></i> NISSAN</a>
                    </li>
                    <li>
                        <a href="opel.php"><i class=""></i> OPEL</a>
                    </li>
                    <li>
                        <a href="isuzu.php"><i class=""></i> ISUZU</a>
                    </li>
                      <li>
                        <a href="toyota.php"><i class=""></i> TOYOTA</a>
                    </li>
                    <li>
                       <a href="chevrolet.php"><i class=""></i> CHEVROLET</a>
                    </li>
                    <li>
                       <a href="gmkorea.php"><i class=""></i> GM KOREA</a>
                    </li>
                    <li>
                       <a href="hyundai.php"><i class=""></i> HYUNDAI</a>
                    </li>
					<li>
                        <a href="kia.php"><i class=""></i> KIA</a>
                    </li>
                    <li>
                        <a href="lub.php"><i class=""></i> LUBRICANTES</a>
                    </li>
                    <li>
                        <a href="bat.php"><i class=""></i> BATERIAS</a>
                    </li>
                    <li>
                        <a href="amp.php"><i class=""></i> AMPOLLETAS</a>
                    </li>
                    <li>
                        <a href="correas.php"><i class=""></i> CORREAS</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        
                           <?php
$dg = new C_DataGrid ("SELECT * FROM srel.dbo.listavehicli", "ID","listavehicli");
$dg-> set_col_readonly("ID");
$dg -> set_col_hidden("ID");
/*$dg -> set_col_title("id", "id");
$dg -> set_col_title("DesSubGr", "DESC SUBGRUPO");


$dg-> set_col_title("CODPROD", "INTERNO");
$dg-> set_col_title("COD", "COD PROV");
$dg-> set_col_title("DES", "DESCRIPCION");
$dg-> set_col_title("UNI", "UNIDAD");
$dg-> set_col_title("MOD", "MODELO");
$dg-> set_col_title("PRO", "PROVEEDOR");
$dg-> set_col_title("EST", "ESTADO");
$dg-> set_col_title("DIA", "DIA PEDIDO");
$dg-> set_col_title("VEN", "VENDEDOR");
$dg-> set_col_title("VALOR", "COSTO");*/
$dg -> enable_rownumbers(true);
$dg-> enable_search(true);
//$dg -> set_sortablerow(true);
$dg -> set_sortname('ID', 'DESC');
//$dg -> set_col_width("DES", 500);
//$dg -> set_col_date("DIA", "d-m-Y", "j/n/Y", "dd-mm-yy");
/*$dg -> set_col_format("VALOR", "integer", array("thousandsSeparator" => ".",
                                                          "defaultValue" => "0")); 
$dg -> set_col_format("MESON", "integer", array("thousandsSeparator" => ".",
                                                          "defaultValue" => "0"));                                                          
$dg -> set_col_edittype('EST', 'select', 'pendiente:Pendiente;cotizar:Cotizar;pedido:Pedido;agotado:Agotado');
//Format a cell based on the specified condition
$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"pedido","css"=> array("color"=>"#ffffff","background-color"=>"green")));

$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"cotizar","css"=> array("color"=>"#ffffff","background-color"=>"blue")));    
        
$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"pendiente","css"=> array("color"=>"#000000","background-color"=>"yellow")));
    
    $dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"agotado","css"=> array("color"=>"#000000","background-color"=>"red")));

//$dg -> set_col_edittype('EST', 'select', 'pedido:Pedido;agotado:Agotado;pendiente:Pendiente');
//$dg-> enable_advanced_search(true);
*/
$dg -> set_theme('aristo');
$dg-> set_dimension(1000,400);

$dg->set_pagesize(400);

$dg->enable_edit("INLINE","CRUD");
// inline edit add new row

$dg->enable_kb_nav(true);
$dg->set_locale('es');

//$dg-> setNavOptions('navigator', array ("cloneToTop"=>true) );
$dg->enable_export('EXCEL');
$dg->enable_debug(false);



$dg -> display();
?>
<script type="text/javascript">
$(function() {
var grid = jQuery("#listavehicli");
grid[0].toggleToolbar();
});
</script>
</body>
<div class="row">
						 <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="index.php">Afinamiento</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-file"></i> Motor
                            </li>
                        </ol>
						</div>
                        
                        
                       
                        
                                    
                        
                    </div>
                </div>
                <!-- /.row -->

            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    


</html>
