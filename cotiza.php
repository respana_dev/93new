<?php
session_start();
error_reporting(E_ALL ^ E_NOTICE);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
$dbcon2 = odbc_connect('COTI', 'sa', 'SRel9300');
if(isset($_SESSION['Matrix'])) {
	$Matrix = $_SESSION['Matrix'];
} else {
	$Matrix = array();
	$_SESSION['Matrix'] = $Matrix;
}

$sql="SELECT max(idCot)+1 AS CodCot FROM Cot_general";
$rs = odbc_exec($dbcon2,$sql);
$row = odbc_fetch_array($rs);
$CodCot = $row['CodCot'];
$idvend = $_SESSION['idvend'];
$idcliente = $_SESSION['idrut'];
$dctoCot = $_SESSION['dctoGen'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Documento sin título</title>

<style type="text/css">
.table {
	font-size: 12px;
	font-weight: bold;
	font-family: Tahoma, Geneva, sans-serif;
}
.tahoma11 {
	font-family: Tahoma, Geneva, sans-serif;
	font-size: 11px;
}
.tahoma12negrita {	font-family: Tahoma, Geneva, sans-serif;
	font-size: 12px;
	font-weight:bold;
}
.tahoma111 {font-family: Verdana, Geneva, sans-serif;
	font-size: 11.5px;
}
</style>
</head>

<body>
<table width="300" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td align="center"><img src="img/logo.jpg" alt="" width="220" height="86" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center"><span class="tahoma12negrita">Cotización <?php echo $CodCot; ?></span></td>
  </tr>
  <tr>
    <td align="center" class="tahoma12negrita">Fecha Emisión: <?php echo date("d-m-Y"); ?></td>
  </tr>
  <tr>
    <td align="center" class="tahoma12negrita">Vendedor: <?php echo $idvend; ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
<table width="300" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><table width="300" border="0" cellpadding="0" cellspacing="0">
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290">&nbsp;</td>
      </tr>
      <?php
$valorTotal=0;
$valTotalSIVA=0;
$i = 1;
foreach($Matrix as $key=>$row) {
	$valorTotal=$row[7] + $valorTotal;
	$sql = "INSERT INTO Cot_detalle VALUES ('$CodCot', '$i','$row[0]','$row[1]','$row[2]','$row[3]','$row[4]','$row[5]','$row[6]','$row[7]')";
	$rs = odbc_exec($dbcon2,$sql);	
	$i++;
	echo '
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290">'.$row[2].'</td>
      </tr>
      <tr class="tahoma111">
        <td>&nbsp;</td>
        <td>
		<table width="290" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100"><strong>'.$row[0].'</strong></td>
            <td width="20"><strong>'.$row[5].'</strong></td>
			<td width="10"><strong>x</strong></td>
            <td align="right"><strong>$ '.number_format($row[4],0,',','.').'&nbsp;</strong></td>
            <td width="10" align="center"><strong>=</strong></td>
            <td width="80" align="right"><strong>$ '.number_format($row[7],0,',','.').'&nbsp;</strong></td>
          </tr>
        </table>
		</td>
      </tr>
      <tr>
        <td height="10"></td>
        <td height="10"></td>
      </tr>
';
  $valTotalSIVA = $valTotalSIVA + $valLineaSIVAS;
}

$sql = "INSERT INTO Cot_general (idCot, fechaCot, dctoCot, valorCot, clienteCot, vendedorCot) VALUES('$CodCot',CONVERT (date, GETDATE()),'$dctoCot','$valorTotal','$idcliente','$idvend')";
$rs = odbc_exec($dbcon2,$sql);
?>
    </table></td>
  </tr>
</table>
<table width="300" border="1" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="300" border="0" cellpadding="0" cellspacing="0">
      <tr class="tahoma111">
        <td width="10"></td>
        <td width="290"><table width="290" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td align="right"><strong>Total&nbsp;</strong></td>
            <td width="10"><strong>=</strong></td>
            <td width="100" align="right"><strong><?php echo '&nbsp;$ '.number_format($valorTotal,0,',','.'); ?></strong>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr class="tahoma111">
        <td width="10">&nbsp;</td>
        <td width="290"></td>
      </tr>
    </table></td>
  </tr>
</table>
<br />
<!--
<0A0A0A0A0A0A0A0A0A0A0A0A>
-->
</body>
</html>
<script>
window.print(false);
</script>