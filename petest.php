		<?php
			require_once("cache.php");
			require_once("conf.php"); 
			include_once("page_template.html");
			include_once("aplicaciones/dbcon.php");
		?>
		<div id="page-wrapper">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-12">
						<?php
							$dg = new C_DataGrid ("SELECT ID, CODPROD, COD, DES, UNI, MOD, PRO, EST, DIA, VEN
												   FROM Srel.dbo.pedidos", "ID", "PEDIDOS");
							
							$dg -> set_theme('aristo');
							$dg -> set_locale('es');
							
							$dg -> set_col_readonly("ID");
							$dg -> set_col_hidden("ID",FALSE);
							
							$dg -> set_col_width("DES", 390);
							$dg -> set_col_width("CODPROD", 150);
							$dg -> set_col_width("COD", 150);
							$dg -> set_col_width("UNI", 120);
							$dg -> set_col_width("DIA", 200);
							$dg -> set_col_width("EST", 180);
							$dg -> set_col_width("PRO", 180);
							
							$dg -> set_col_title("CODPROD", "Producto");
							$dg -> set_col_title("COD", "Proveedor");
							$dg -> set_col_title("DES", "Descripcion");
							$dg -> set_col_title("UNI", "Cantidad");
							$dg -> set_col_title("MOD", "Modelo");
							$dg -> set_col_title("PRO", "Nom. Proveedor");
							$dg -> set_col_title("EST", "Estado");
							$dg -> set_col_title("DIA", "Fecha");
							$dg -> set_col_title("VEN", "Vendedor");
							
							$dg -> set_sortname('ID', 'DESC');
							$dg -> enable_export('excel');
							$dg -> enable_search(true);
							$dg -> set_col_edittype('EST', 'select', 'pendiente:Vta_Directa;cotizar:Cotizar;pedido:Pedido;reposicion:Reposicion;agotado:Agotado;recibido:Recibido');

							$dg -> set_conditional_format("EST","CELL",array("condition"=>"eq","value"=>"pedido","css"=> array("color"=>"#ffffff","background-color"=>"green")));
							$dg -> set_conditional_format("EST","CELL",array("condition"=>"eq","value"=>"cotizar","css"=> array("color"=>"#ffffff","background-color"=>"blue")));    
							$dg -> set_conditional_format("EST","CELL",array("condition"=>"eq","value"=>"pendiente","css"=> array("color"=>"#000000","background-color"=>"yellow")));
							$dg -> set_conditional_format("EST","CELL",array("condition"=>"eq","value"=>"agotado","css"=> array("color"=>"#000000","background-color"=>"red")));
							$dg -> set_conditional_format("EST","CELL",array("condition"=>"eq","value"=>"recibido","css"=> array("color"=>"#000000","background-color"=>"orange")));  
							$dg -> set_conditional_format("EST","CELL",array("condition"=>"eq","value"=>"reposicion","css"=> array("color"=>"#000000","background-color"=>"purple")));
							
							$dg -> set_pagesize(30);
							$dg -> enable_debug(true);
							$dg -> set_dimension(1080, 450);
							$dg -> set_multiselect(true, true);
							$dg -> display();
						?>
						<div id="contMain" name="contMain" style="height:1px;width:1px">
						</div>
					</div>
				</div>
			</div>
		</div>
	<!-- jQuery -->
		<script src="js/jquery.js"></script>
	<!-- Bootstrap Core JavaScript -->
		<script src="js/bootstrap.min.js"></script>
		<script language="javascript">
			$('#page-wrapper').click(function(){
				var rows = getSelRows();
				$("#contMain").load("mainped.php?cod="+rows);
			});
		</script>
	</body>
</html>