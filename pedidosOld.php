<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>PEDIDOS</title>
</head>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>

<body>
<?php include_once("header.php"); ?>


<div id="wrapper">
 <?php
require_once("conf.php");



//$marca=$_GET['marca'];
  
//realizamos la conexion mediante odbc
//$cid=odbc_connect($dsn, $usuario, $clave);
$cid = odbc_connect('SREL', 'sa', 'SRel9300');

if (!$cid){
	exit("<strong>Ya ocurrido un error tratando de conectarse con el origen de datos.</strong>");
}	

// despues de generar la tabla de marcas se despliega el listado de productos de la marca selec

//echo $_GET['marca'];
if (isset($_REQUEST['marca'])) {
 $marca = $_REQUEST['marca'];
} else {
 $marca = 80;
}

$dg = new C_DataGrid ("SELECT * FROM pedidos", "ID","PEDIDOS");
$dg -> set_col_title("id", "id");
$dg -> set_col_title("DesSubGr", "DESC SUBGRUPO");
$dg -> set_col_readonly("ID");
$dg -> set_col_hidden('ID',FALSE);
$dg-> set_col_title("CODPROD", "INTERNO");
$dg-> set_col_title("COD", "COD PROV");
$dg-> set_col_title("DES", "DESCRIPCION");
$dg-> set_col_title("UNI", "UNIDAD");
$dg-> set_col_title("MOD", "MODELO");
$dg-> set_col_title("PRO", "PROVEEDOR");
$dg-> set_col_title("EST", "ESTADO");
$dg-> set_col_title("DIA", "DIA PEDIDO");
$dg-> set_col_title("VEN", "VENDEDOR");
$dg-> set_col_title("VALOR", "COSTO");
//$dg -> enable_rownumbers(true);
$dg-> enable_search(true);
//$dg -> enable_global_search(true);
//$dg -> set_sortablerow(true);
$dg -> set_sortname('ID', 'DESC');
$dg -> set_col_width("DES", 500);
//$dg -> set_col_date("DIA", "d-m-Y", "j/n/Y", "dd-mm-yy");
$dg -> set_col_format("VALOR", "integer", array("thousandsSeparator" => ".",
                                                          "defaultValue" => "0")); 
$dg -> set_col_format("MESON", "integer", array("thousandsSeparator" => ".",
                                                          "defaultValue" => "0"));                                                          
//$dg -> set_col_edittype('EST', 'select', 'pendiente:Pendiente;cotizar:Cotizar;pedido:Pedido;agotado:Agotado');
$dg -> set_col_edittype('EST', 'select', 'pendiente:Vta_Directa;cotizar:Cotizar;pedido:Pedido;reposicion:Reposicion;agotado:Agotado;recibido:Recibido');
//Format a cell based on the specified condition
$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"pedido","css"=> array("color"=>"#ffffff","background-color"=>"green")));

$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"cotizar","css"=> array("color"=>"#ffffff","background-color"=>"blue")));    
        
$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"pendiente","css"=> array("color"=>"#000000","background-color"=>"yellow")));
    
$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"agotado","css"=> array("color"=>"#000000","background-color"=>"red")));
    
$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"recibido","css"=> array("color"=>"#000000","background-color"=>"orange")));  

$dg->set_conditional_format("EST","CELL",array(
    "condition"=>"eq","value"=>"reposicion","css"=> array("color"=>"#000000","background-color"=>"purple")));  

//$dg -> set_col_edittype('EST', 'select', 'pedido:Pedido;agotado:Agotado;pendiente:Pendiente');
//$dg-> enable_advanced_search(true);
$dg -> set_theme('aristo');
$dg -> set_dimension(1020,450);

$dg->set_pagesize(400);
//$dg-> set_col_dynalink("COD","http://IIS/93/match.php","COD");
$dg-> set_col_dynalink("COD","http://IIS/93/match.php",array("COD", "PRO","DES"));
$dg->enable_edit("FORM","CRUD");

// inline edit add new row

$dg->enable_kb_nav(true);
$dg->set_locale('es');

//$dg-> setNavOptions('navigator', array ("cloneToTop"=>true) );
$dg->enable_export('EXCEL');
$dg->enable_debug(FALSE);



$dg -> display();

?>

<div id="headerwrap">
<table width="10%" border="0" align="" cellpadding="0" cellspacing="0">
  <tr class="">

    <td width="10" id="header"><table width="0%" border="0" cellspacing="0" cellpadding="0">
        
       
        <tr>
          
        </tr>
        
      </table></td>
    <td width="400">&nbsp;</td>
    <td id="header" width="300px"><table width="" border="0" cellspacing="0" cellpadding="0">
       
        
    <td width="300">&nbsp;</td>
    <td width="300" align="" id="header"><table width="" border="0" cellspacing="0" cellpadding="0">
      <tr>
       


?>
      </tr>
     
      <tr>
       
      </td>
      </tr>
    </table></td>
    <td width="10">&nbsp;</td>
    <td width="0" align="" id="header"><table width="0" border="0" cellspacing="0" cellpadding="0">
      <tr>
        
      </tr>
       
     <tr>
        
      </tr>
    </table></td>
   
  </tr>
</table>

 
</div>
</div>

</body>
</html>